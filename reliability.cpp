/* 
 * File:   reliability.cpp
 * Author: rafaellgom
 * 
 * Created on 29 de Setembro de 2012, 14:33
 */

#include "topologyManager.h"
#include "reliability.h"

Reliability::Reliability() {

}

Reliability::Reliability(std::list<node*>* d, node* r) {
    dest = d;
    starter = r;
}

Reliability::Reliability(topologyInfo* t, std::list<node*>* d, node* r) {
    topology = t;
    dest = d;
    starter = r;
    //m = numberMostProbableStates(1);
    //printf("m %d\n",m);
    states = generateNetworkStatesNew(topology);
    n = topology->nodes->size() + topology->links->size();
    //mProbStates = orderAlgorithm(states, m);
    finalReliab = finalReliabilityNew(states, topology);
}

Reliability::Reliability(topologyInfo* t, std::list<node*>* d, node* r, int fails) {
    topology = t;
    dest = d;
    starter = r;
    //m = numberMostProbableStates(fails);
    //printf("m %d\n",m);
    states = generateNetworkStates(topology);
    n = topology->nodes->size() + topology->links->size();
    //mProbStates = orderAlgorithm(states, m);
    finalReliab = finalReliability(states, topology);
}

Reliability::~Reliability() {
    //delete topology;    
    delete states;
    //    for (std::list<networkState*>::iterator it = mProbStates->begin(); it != mProbStates->end(); it++) {
    //        for (std::vector<networkComponent*>::iterator i = (*it)->state.end(); i < (*it)->state.begin(); i--) {
    //            delete *i;
    //        }
    //        mProbStates->erase(it);
    //        it--;
    //    }
    //delete mProbStates;
    //delete dest;
    //delete starter;
    //printf("finish...\n");
}

bool compareNetSt(networkState* a, networkState* b) {
    if (a->prob > b->prob)
        return true;
    else
        return false;
}

bool compareNetId(networkState* a, networkState* b) {
    if (a->id == b->id)
        return true;
    else
        return false;
}

std::vector<int> Reliability::intToBinary(int number) {
    std::vector<int> out; // = new std::vector<int>();
    int remainder;
    if (number <= 1) {
        out.push_back(number);
        return out;
    }
    remainder = number % 2;
    out.push_back(remainder);
    std::vector<int> aux = intToBinary(number >> 1);
    out.insert(out.end(), aux.begin(), aux.end());
    return out;
    //cout << remainder;
}

long Reliability::binaryToInt(std::vector<networkComponent*> vet) {
    long out = vet[0]->active;
    //printf("v[%d] %d ", 0, vet[0]->active);
    for (int i = 1; i < vet.size(); i++) {
        //printf("v[%d] %d ", i, vet[i]->active);
        //int aux = 
        if (vet[i]->active == 1) {
            out += (pow(pow(2, i), vet[i]->active));
        }
    }
    //printf("\n");
    return out;
}

std::list<networkState*>* Reliability::generateNetworkStatesNew(topologyInfo* t) {

    std::list<networkState*>* possibleStates = new std::list<networkState*>();
    networkState* s = new networkState();

    int index = 0;
    for (std::list<node*>::iterator i = t->nodes->begin(); i != t->nodes->end(); i++) {
        (*i)->type = SWITCHS;
        //printf("Info node: id %d type %d rel %f \n", (*i)->id, (*i)->type, (*i)->reliability);
        s->state.push_back(*i);
        s->state[index]->type = SWITCHS;
        index++;
    }

    for (std::list<linkInfo*>::iterator i = t->links->begin(); i != t->links->end(); i++) {
        (*i)->type = LINK;
        //                                printf("Info link: from %d to %d type %d rel %f w %f \n",
        //                                        (*i)->from->id, (*i)->to->id, (*i)->type, (*i)->reliability, (*i)->energy);
        s->state.push_back(*i);
    }

    for (int i = 0; i < s->state.size() + 1; i++) {
        networkState* st = new networkState();
        (*st) = *s;
        //printf("State %d :: ", i);
        for (int j = 0; j < st->state.size(); j++) {
            //st->state[j] = new networkComponent();
            //*st->state[j] = *s->state[j];

            if (s->state[j]->type == LINK) {
                linkInfo *l = new linkInfo();
                *l = static_cast<linkInfo&> (*s->state[j]);
                *st->state[j] = *l;
                delete l;
            } else {
                node* no = new node();
                *no = static_cast<node&> (*s->state[j]);
                *st->state[j] = *no;
                delete no;
            }

            if (j == i) {
                st->state[j]->active = 1;
                st->operational.push_back(1);
            } else {
                st->state[j]->active = 0;
                st->operational.push_back(0);
            }

        }

        st->prob = 1;
        for (int j = 0; j < st->state.size(); j++) {
            st->prob *= st->state[j]->reliability * pow(((1 - st->state[j]->reliability) / (st->state[j]->reliability)), st->operational[j]);
            //printf("(%d == %d )dev %d act %d :: prob %f -> %f\n", i,j,st->state[j]->id, st->operational[j], st->prob, 
            //st->state[j]->reliability * pow(((1 - st->state[j]->reliability) / (st->state[j]->reliability)), st->state[j]->active));
        }//fim do for j

        st->id = i;
        //printf("%d :: prob %f \n", st->id, st->prob);
        possibleStates->push_back(st);
    }
    //printf("end %d\n", possibleStates->size());
    return possibleStates;
}

std::list<networkState*>* Reliability::generateNetworkStates(topologyInfo* t) {

    std::list<networkState*>* possibleStates = new std::list<networkState*>();
    networkState* s = new networkState();

    int index = 0;
    for (std::list<node*>::iterator i = t->nodes->begin(); i != t->nodes->end(); i++) {
        (*i)->type = SWITCHS;
        //        printf("Info node: id %d type %d rel %f \n", (*i)->id, (*i)->type, (*i)->reliability);
        s->state.push_back(*i);
        s->state[index]->type = SWITCHS;
        index++;
    }

    for (std::list<linkInfo*>::iterator i = t->links->begin(); i != t->links->end(); i++) {
        (*i)->type = LINK;
        //                        printf("Info link: from %d to %d type %d rel %f w %f \n",
        //                                (*i)->from->id, (*i)->to->id, (*i)->type, (*i)->reliability, (*i)->weight);
        s->state.push_back(*i);
    }

    for (int i = 0; i < pow(2, s->state.size()); i++) {
        networkState* st = new networkState();
        (*st) = *s;
        std::vector<int> temp = intToBinary(i);
        //printf("State %d :: ", i);
        for (int j = 0; j < st->state.size(); j++) {
            //st->state[j] = new networkComponent();
            //*st->state[j] = *s->state[j];

            if (s->state[j]->type == LINK) {
                linkInfo *l = new linkInfo();
                *l = static_cast<linkInfo&> (*s->state[j]);
                *st->state[j] = *l;
                delete l;
            } else {
                node* no = new node();
                *no = static_cast<node&> (*s->state[j]);
                *st->state[j] = *no;
                delete no;
            }

            if (j < temp.size()) {
                st->state[j]->active = temp[j];
            } else {
                st->state[j]->active = 0;
            }
            //printf("%d \n", st->state[j]->id);
        }//fim do for j
        //printf("\n");

        st->prob = 1;
        st->id = i;

        for (int j = 0; j < st->state.size(); j++) {
            st->prob *= st->state[j]->reliability * pow(((1 - st->state[j]->reliability) / (st->state[j]->reliability)), st->state[j]->active);
        }//fim do for j

        possibleStates->push_back(st);

        //if (i == m)
        return possibleStates;
        //numberOfCommunicateNodes(st, t);
    }//fim do for i

    return possibleStates;
}

std::list<networkState*>* Reliability::orderAlgorithm(std::list<networkState*>* u, int m) {

    int iLinha = ceil(log2(m));
    //printf("i\' = %d \n", iLinha);

    std::list<networkState*>* A = new std::list<networkState*>(), *B = new std::list<networkState*>();

    A = u;

    for (int i = 1; i < iLinha + 1; i++) {
        B = appendAi(A, i);

        A = AsetaB(B, A);

        for (std::list<networkState*>::iterator j = B->begin(); j != B->end(); j++) {
            //                        for (std::vector<networkComponent*>::iterator k = (*j)->state.begin(); k != (*j)->state.end(); k++) {
            //                            (*j)->state.erase(k);
            //                            k--;
            //                        }
            B->erase(j);
            j--;
        }
    }

    for (std::list<networkState*>::iterator j = B->begin(); j != B->end(); j++) {
        for (std::vector<networkComponent*>::iterator k = (*j)->state.begin(); k != (*j)->state.end(); k++) {
            (*j)->state.erase(k);
            k--;
        }
        B->erase(j);
        j--;
    }

    A = TmA(A, m);

    for (int i = iLinha + 1; i < n; i++) {
        B = appendAi(A, i);

        A = AsetaB(B, A);

        for (std::list<networkState*>::iterator j = B->begin(); j != B->end(); j++) {
            for (std::vector<networkComponent*>::iterator k = (*j)->state.begin(); k != (*j)->state.end(); k++) {
                (*j)->state.erase(k);
                k--;
            }
            B->erase(j);
            j--;
        }

        A = TmA(A, m);
    }

    delete B;

    return A;
}

int Reliability::numberMostProbableStates(int L) {

    int m;
    double fl = 0, fl2 = 0, fl3 = 0;
    int n = topology->numberUnreliable;
    double q = topology->minProb;
    double p = 1 - q;

    //calcula fl
    for (int k = 0; k < L; k++) {
        fl += comb(n, k) * pow(p, n - k) * pow(q, k);
    }

    //
    int k = 1;
    double sum = pow(p, n);
    while (fl > sum) {
        sum += comb(n, k) * pow(p, n - k) * pow(q, k);
        k++;
    }
    int L2 = k - 1;

    //calcula fl'
    for (int k = 0; k < L2; k++) {
        fl2 += comb(n, k) * pow(p, n - k) * pow(q, k);
    }

    if (fl == fl2) {
        m = comb(n, L2);
    } else {//fl < fl'

        double x;

        //calcula fl'-1
        for (int k = 0; k < L2 - 1; k++) {
            fl3 += comb(n, k) * pow(p, n - k) * pow(q, k);
        }

        x = (fl - fl3) / (pow(p, n - L2) * pow(q, L2));

        for (int k = 0; k < L2 - 1; k++) {
            x += comb(n, k);
        }

        m = ceil(x);
        //printf("ceil(%f) = %d \n",x,m);
    }

    //printf("Number:: ProbStates:: %d | num %d\n", (m/L) ,n);

    return (int) (m / (L * L * L));
}

int Reliability::fatorial(int n, int stop) {
    int t = 1;
    for (int i = n; i >= stop; i--) {
        t *= i;
    }
    //printf("fat(%d until %d) = %d \n",n, stop,t);
    return t;
}

double Reliability::comb(int n, int p) {
    double t;

    printf("n %d p %d \n", n, p);
    if (p > n - p) {
        t = fatorial(n, p) / fatorial(n - p, 1);
    } else {
        t = fatorial(n, n - p) / fatorial(p, 1);
    }

    //t = fatorial(n,1) / (fatorial(p,1) * fatorial(n - p,1));
    //printf("comb(%d,%d) = %f \n",n,p,t);
    return t;
}

int Reliability::CommDestination(networkState* n, topologyInfo* topo) {

    topologyInfo* t = adjustTopology(n, topo);

    if (t->nodes->size() < 2 || t->links->size() < 1) {
        //printf("State %d :: Insufficient number of components to exist communication\n", n->id);
        return 0;
    }

    //std::cin.ignore().get();

    int numberCommNodes = 0;

    //printf("Original Size: nodes %d links %d \n", topo->nodes->size(), topo->links->size());
    //printf("New Size: nodes %d links %d \n", t->nodes->size(), t->links->size());

    //        printf("Nodes :: ");
    //        for (std::list<node *>::iterator i = t->nodes->begin(); i != t->nodes->end(); i++) {
    //            printf("%d ", (*i)->id);
    //        }
    //        printf("\n");
    //    
    //        printf("Links :: ");
    //        for (std::list<linkInfo*>::iterator i = t->links->begin(); i != t->links->end(); i++) {
    //            printf("%d -> %d , ", (*i)->from->id, (*i)->to->id);
    //        }
    //        printf("\n");

    std::map < node*, bool> processed;
    std::map < node*, bool> discovered;

    node * start = new node;
    start->id = -9999;

    for (std::list<node *>::iterator i = t->nodes->begin(); i != t->nodes->end(); i++) {
        if ((*i)->id == starter->id) {
            start = *i;
        }

        processed.insert(std::pair < node*, bool>(*i, false));
        discovered.insert(std::pair < node*, bool>(*i, false));
    }

    if (start->id == -9999) {
        return 0;
    }

    //node * start = (*t->nodes->begin());
    std::queue<node *> q;
    node * v;

    for (std::list<node *>::iterator i = t->nodes->begin(); i != t->nodes->end(); i++) {
        discovered[*i] = false;
    }

    q.push(start);
    processed[start] = true;
    discovered[start] = true;
    //printf("Start Node %d\n", start->id);

    while (q.empty() == false) {
        v = q.front();
        q.pop();
        //printf("Processing vertex %d\n", v->id);
        for (std::list<node*>::iterator i = t->nodes->begin(); i != t->nodes->end(); i++) {

            if ((*i)->id == v->id) {
                continue;
            }

            //printf("*current node %d \n", (*i)->id);
            bool linkFound = false;
            for (std::list<linkInfo*>::iterator link = t->links->begin(); link != t->links->end(); link++) {
                //printf("link:: %d -> %d \n", (*link)->from->id, (*link)->to->id);
                if ((((*link)->from->id == v->id) && (*link)->to->id == (*i)->id) ||
                        (((*link)->to->id == v->id) && (*link)->from->id == (*i)->id)) {
                    linkFound = true;
                    //printf("found \n");
                    break;
                }
            }

            if (linkFound == true) {
                //printf("find edge %d -> %d bool %s\n", v->id, (*i)->id, discovered[v] ? "true" : "false");
                if (discovered[*i] == false) {
                    q.push(*i);
                    discovered[*i] = true;
                    for (std::list<node*>::iterator d = dest->begin(); d != dest->end(); d++) {
                        if ((*d)->id == (*i)->id) {
                            numberCommNodes++;
                            break;
                        }
                    }
                }
            }//
        }//for i

    }//while

    //printf("Number of communicate pair of nodes %d \n ////--------\n", numberCommNodes);
    //for (std::list<node *>::iterator i = t->nodes->begin(); i != t->nodes->end(); i++) {
    //delete *i;
    //}
    //for (std::list<linkInfo*>::iterator link = t->links->begin(); link != t->links->end(); link++) {
    //    delete *link;
    //}
    //delete t;

    for (std::vector<networkComponent*>::iterator j = n->state.begin(); j != n->state.end(); j++) {
        n->state.erase(j);
        j--;
    }

    return numberCommNodes;
}

int Reliability::numberOfCommunicateNodes(networkState* n, topologyInfo* topo) {
    topologyInfo* t = adjustTopology(n, topo);

    if (t->nodes->size() < 2 || t->links->size() < 1) {
        //printf("State %d :: Insufficient number of components to exist communication\n", n->id);
        return 0;
    }

    //std::cin.ignore().get();

    int numberCommNodes = 0;

    //    printf("Original Size: nodes %d links %d \n", topo->nodes->size(), topo->links->size());
    //    printf("New Size: nodes %d links %d \n", t->nodes->size(), t->links->size());

    //        printf("Nodes :: ");
    //        for (std::list<node *>::iterator i = t->nodes->begin(); i != t->nodes->end(); i++) {
    //            printf("%d ", (*i)->id);
    //        }
    //        printf("\n");
    //    
    //        printf("Links :: ");
    //        for (std::list<linkInfo*>::iterator i = t->links->begin(); i != t->links->end(); i++) {
    //            printf("%d -> %d , ", (*i)->from->id, (*i)->to->id);
    //        }
    //        printf("\n");

    std::map < node*, bool> processed;
    std::map < node*, bool> discovered;

    for (std::list<node *>::iterator i = t->nodes->begin(); i != t->nodes->end(); i++) {
        processed.insert(std::pair < node*, bool>(*i, false));
        discovered.insert(std::pair < node*, bool>(*i, false));
    }

    node * start = (*t->nodes->begin());
    std::queue<node *> q;
    node * v;

Continue:

    for (std::list<node *>::iterator i = t->nodes->begin(); i != t->nodes->end(); i++) {
        discovered[*i] = false;
    }

    q.push(start);
    processed[start] = true;
    discovered[start] = true;
    //printf("Start Node %d\n", start->id);

    while (q.empty() == false) {
        v = q.front();
        q.pop();
        //printf("Processing vertex %d\n", v->id);
        for (std::list<node*>::iterator i = t->nodes->begin(); i != t->nodes->end(); i++) {

            if ((*i)->id == v->id) {
                continue;
            }

            //printf("*current node %d \n", (*i)->id);
            bool linkFound = false;
            for (std::list<linkInfo*>::iterator link = t->links->begin(); link != t->links->end(); link++) {
                //printf("link:: %d -> %d \n", (*link)->from->id, (*link)->to->id);
                if ((((*link)->from->id == v->id) && (*link)->to->id == (*i)->id) ||
                        (((*link)->to->id == v->id) && (*link)->from->id == (*i)->id)) {
                    linkFound = true;
                    //printf("found \n");
                    break;
                }
            }

            if (linkFound == true) {
                //printf("find edge %d -> %d bool %s\n", v->id, (*i)->id, discovered[v] ? "true" : "false");
                if (discovered[*i] == false) {
                    q.push(*i);
                    discovered[*i] = true;
                    numberCommNodes++;
                }
            }//
        }//for i

        if (q.empty()) {

            for (std::map < node*, bool>::iterator d = processed.begin(); d != processed.end(); d++) {
                if ((*d).second == false) {
                    //printf("Queue finished and node %d not found\n", (*d).first->id);
                    start = (*d).first;
                    goto Continue;
                }
            }
        }//if q.empty

    }//while

    //printf("Number of communicate pair of nodes %d \n ////--------\n", numberCommNodes);

    for (std::vector<networkComponent*>::iterator j = n->state.begin(); j != n->state.end(); j++) {
        n->state.erase(j);
        j--;
    }

    return numberCommNodes / 2;
}

topologyInfo* Reliability::adjustTopology(networkState* n, topologyInfo* topo) {

    topologyInfo* t = new topologyInfo();
    t->links = new std::list<linkInfo*>();
    t->nodes = new std::list<node*>();
    (*t->links) = (*topo->links);
    (*t->nodes) = (*topo->nodes);

    //std::cin.ignore().get();
    //if (n->id < t->links->size() + t->nodes->size())
    //printf("erro %d \n", n->state[n->id]->id);

    for (int i = 0; i < n->state.size(); i++) {

        if (n->state[i]->active == 0) {
            //printf("Component %d active\n", n->state[i]->id);
            continue;
        }

        //printf("Type %d i %d act %d %.20f \n", n->state[i]->type, i, n->state[i]->active, n->prob);

        if (n->state[i]->type == LINK) {
            linkInfo l;
            networkComponent* ncomp = n->state[i];
            try {
                l = static_cast<linkInfo&> (*ncomp);
            } catch (...) {
            }

            for (std::list<linkInfo*>::iterator j = t->links->begin(); j != t->links->end(); j++) {
                //printf("Link: %d -> %d ??? \n", (*j)->from->id, (*j)->to->id);
                if (((*j)->from->id == l.from->id) && ((*j)->to->id == l.to->id)) {
                    //printf("Link: %d -> %d found \n", l.from->id, l.to->id);
                    t->links->erase(j);
                    break;
                }
            }//for j

        } else { //n->state[j]->type == SWITCHS

            for (std::list<node*>::iterator j = t->nodes->begin(); j != t->nodes->end(); j++) {
                if ((*j)->id == n->state[i]->id) {
                    //printf("Node %d found \n", n->state[i]->id);
                    t->nodes->erase(j);

                    //apaga todos os links com esse noh
                    for (std::list<linkInfo*>::iterator k = t->links->begin(); k != t->links->end(); k++) {
                        if (((*j)->id == (*k)->from->id) || ((*j)->id == (*k)->to->id)) {
                            //printf("Link: %d -> %d f \n", (*k)->from->id, (*k)->to->id);
                            t->links->erase(k);
                            //break;
                            k--; // = t->links->begin();
                        }
                    }//for j

                    break;
                }
            }//for j

        }//fim do else type

    }// for i   

    //tira os links q o noh nao esta na topologia
    for (std::list<linkInfo*>::iterator k = t->links->begin(); k != t->links->end(); k++) {
        bool linkFoundFrom = false;
        bool linkFoundTo = false;
        //printf("Link: %d -> %d f \n", (*k)->from->id, (*k)->to->id);
        for (std::list<node*>::iterator j = t->nodes->begin(); j != t->nodes->end(); j++) {
            //printf("\tNode %d\n",(*j)->id);
            if ((*j)->id == (*k)->from->id) {
                linkFoundFrom = true;
            }//if                
            if ((*j)->id == (*k)->to->id) {
                linkFoundTo = true;
            }//if                
        }//j
        if ((linkFoundFrom == false) || (linkFoundTo == false)) {
            t->links->erase(k);
            k--; // = t->links->begin();
        }
    }//for k

    for (std::list<node*>::iterator j = t->nodes->begin(); j != t->nodes->end(); j++) {
        bool nodeFound = false;
        for (std::list<linkInfo*>::iterator k = t->links->begin(); k != t->links->end(); k++) {
            if (((*j)->id == (*k)->from->id) || ((*j)->id == (*k)->to->id)) {
                nodeFound = true;
                break;
            }//if                
        }//j     
        if (nodeFound == false) {
            t->nodes->erase(j);
            j--; // = t->nodes->begin();
        }
    }//for k

    //        for (std::list<node*>::iterator i = t->nodes->begin(); i != t->nodes->end(); i++) {
    //            printf("\tNode %d found \n", (*i)->id);
    //        }
    //    
    //        for (std::list<linkInfo*>::iterator i = t->links->begin(); i != t->links->end(); i++) {
    //            printf("\tLink: %d -> %d found \n", (*i)->from->id, (*i)->to->id);
    //        }

    for (std::vector<networkComponent*>::iterator j = n->state.begin(); j != n->state.end(); j++) {
        n->state.erase(j);
        j--;
    }

    return t;
}

std::list<networkState*>* Reliability::AsetaB(std::list<networkState*>* A, std::list<networkState*>* B) {
    std::list<networkState*>* seta = new std::list<networkState*>();
    std::list<networkState*>* aux = new std::list<networkState*>();
    (*seta) = *B;
    (*aux) = *A;

    seta->merge(*aux, compareNetId);

    for (std::list<networkState*>::iterator it = aux->begin(); it != aux->end(); it++) {
        aux->erase(it);
        it--;
    }
    delete aux;



    return seta;
}

std::list<networkState*>* Reliability::appendAi(std::list<networkState*>* A, int i) {
    std::list<networkState*>* out = new std::list<networkState*>();

    for (std::list<networkState*>::iterator it = A->begin(); it != A->end(); it++) {
        networkState* st = new networkState();
        st->prob = 1;
        st->state = (*it)->state;
        st->id = (*it)->id;
        //printf("i %d type %d \n", i, (st)->state.size());
        if ((st)->state.size() <= i) {
            continue;
        }
        //printf("bin %d \n",(*it)->state[i]->active);
        if ((*it)->state[i]->active == 0) {

            if ((*it)->state[i]->type == LINK) {
                //st->state[i] = new linkInfo();
                //*(st->state[i]) = *(*it)->state[i];
                try {
                    linkInfo* l = new linkInfo();
                    *l = static_cast<linkInfo&> (*(*it)->state[i]);
                    //printf("%d -> %d \n", l->from->id, l->to->id);
                    st->state[i] = l;
                } catch (...) {
                }

            } else {
                st->state[i] = new networkComponent();
                *(st->state[i]) = *(*it)->state[i];
            }

            st->state[i]->active = 1;

            for (int j = 0; j < st->state.size(); j++) {
                st->prob *= st->state[j]->reliability * pow(((1 - st->state[j]->reliability) / (st->state[j]->reliability)), st->state[j]->active);
            }//fim do for j

            //st->prob = (*it)->prob * (*it)->state[i]->reliability * ((1 - (*it)->state[i]->reliability) / (*it)->state[i]->reliability);
            //printf("i %d st %d \t", i, st->id);
        }
        st->id = binaryToInt(st->state);
        //out->push_back(st);
        if ((*it)->prob > 0.0000000001)
            out->push_back(st);
        else {
            for (std::vector<networkComponent*>::iterator j = st->state.begin(); j != st->state.end(); j++) {
                st->state.erase(j);
                j--;
            }
            delete st;
        }


    }//fim do for

    return out;
}

std::list<networkState*>* Reliability::TmA(std::list<networkState*>* A, int m) {
    //    std::list<networkState*>* out = new std::list<networkState*>();
    //    *out = *A;
    A->sort(compareNetSt);
    if (A->size() > m) {
        //A->resize(m);
        int s = 0;
        for (std::list<networkState*>::iterator it = A->begin(); it != A->end(); it++) {
            if (s > m) {
                A->erase(it);
                //delete *it;
                it--;
            }
            s++;
        }

    }
    return A;
}

double Reliability::finalReliability(std::list<networkState*>* s, topologyInfo* t) {

    double reliability = 0, Ex = 0, ExLinha = 0, prob = 0, Xu = 0, Xl = 0, Cu = 0, Cl = 0;

    for (std::list<networkState*>::iterator i = s->begin(); i != s->end(); i++) {
        //printf("state %d :: \t", dest->size());
        //int comNum = numberOfCommunicateNodes(*i, t);
        int comNum = CommDestination(*i, t);
        Ex += ((*i)->prob * comNum);

        if (comNum == dest->size()) {
            Cl += (*i)->prob;
        }
        prob += (*i)->prob;

        for (std::vector<networkComponent*>::iterator j = (*i)->state.begin(); j != (*i)->state.end(); j++) {
            (*i)->state.erase(j);
            j--;
        }
    }

    Xu = Ex + ((1 - prob) * dest->size());
    Xl = Ex + ((1 - prob) * 0);

    //printf("ExpectedCommun:: Xl %f Xu %f\n", Xl, Xu);

    Cu = Cl + (1 - prob);

    //printf("ProbConnected:: CommLow %f CommUp %f\n", Cl, Cu);
    reliability = (Cl + Cu) / 2;

    return reliability;
}

double Reliability::finalReliabilityNew(std::list<networkState*>* s, topologyInfo* t) {

    double reliability = 0, prob = 0, Cu = 0, Cl = 0;
    for (std::list<networkState*>::iterator i = s->begin(); i != s->end(); i++) {

        //printf("state %d :: \t", (*i)->id);
        int index = 0;
        for (std::vector<networkComponent*>::iterator j = (*i)->state.begin(); j != (*i)->state.end(); j++) {
            (*j)->active = (*i)->operational[index];
            index++;
        }

        int comNum = CommDestination(*i, t);
        //((*i)->prob * comNum)
        //reliability += CommDestination(*i, t)/dest->size();
        //printf("comm d %d - %d : %f\n", comNum, dest->size(), (*i)->prob);
        if (comNum == dest->size()) {
            Cl += (*i)->prob;
        }
        prob += (*i)->prob;

        //for (std::vector<networkComponent*>::iterator j = (*i)->state.begin(); j != (*i)->state.end(); j++) {
        //(*i)->state.erase(j);
        //j--;
        //}
    }

    Cu = Cl + (1 - prob);
    reliability = (Cl + Cu) / 2;
    //printf("reliability %f \n", reliability);
    return Cl;
}