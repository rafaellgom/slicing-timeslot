/* 
 * File:   ResquestGeneration.cpp
 * Author: rafaellgom
 * 
 * Created on July 11, 2013, 3:10 PM
 */

#include <iostream>
#include <string>
#include <sstream>  
#include "ResquestGeneration.h"

double exponential(double mean) {

    struct timeval tv; // C requires "struct timval" instead of just "timeval"
    gettimeofday(&tv, 0);

    // use BOTH microsecond precision AND pid as seed
    long int n = tv.tv_usec * getpid();
    srand(n);
    //srand(time(NULL));

    double x = ((double) (rand() % 100000000) / 100000000);

    return (-mean)*(log(x));
}

void ResquestGeneration::generateFailureOrder(topologyInfo* t) {
    int number = 0;

    for (std::list<linkInfo*>::iterator i = t->links->begin(); i != t->links->end(); i++) {
        //pula o link inverso, pois nao precisa contar 2x, e eles vem na sequencia
        //i++;
        failureOrder.push_back(*i);
        number++;
    }

    std::random_shuffle(failureOrder.begin(), failureOrder.end());
}

ResquestGeneration::ResquestGeneration(int r) {
    fixedRel = r;
    listRequest = new std::list<Request*>();
}

ResquestGeneration::~ResquestGeneration() {
}

void ResquestGeneration::loadFile(char* file, std::list<node *> listNodes, topologyInfo* t) {

    std::ifstream in(file);

    std::string line;

    getline(in, line);
    std::stringstream ss(line);
    std::string buf;
    std::getline(ss, buf, ' ');
    while (std::getline(ss, buf, ' ')) {
        failureOrder.push_back(getComponent(t, atoi(buf.c_str())));
    }

    while (getline(in, line)) {
        Request* r = new Request;
        std::stringstream ss(line);
        std::string buf;

        //# of tokens id bw rel source num_dest dest 
        std::getline(ss, buf, ' ');
        //        printf("id: %s \n", buf.c_str());
        r->id = atoi(buf.c_str());

        for (int k = 0; k < NUMBER_OF_SLOTS; k++) {
            std::getline(ss, buf, ' ');
            //            printf("bw (%d): %s \n", k, buf.c_str());
            r->bw.push_back(atof(buf.c_str()));
        }

        std::getline(ss, buf, ' ');
        std::getline(ss, buf, ' ');
        //        printf("reliability: %s \n", buf.c_str());
        r->reliability = atof(buf.c_str());

        std::getline(ss, buf, ' ');
        //        printf("duration: %s \n", buf.c_str());
        r->duration = atoi(buf.c_str());

        std::getline(ss, buf, ' ');
        //        printf("source: %s \n", buf.c_str());
        r->source = getNode(listNodes, (char *) buf.c_str());
        //        printf("Buffer: %s \n", buf.c_str());
        //        printf("Buffer: %s \n", r->source->name);

        std::getline(ss, buf, ' ');
        int numberOfDest = atoi(buf.c_str());
        //        printf("n dest: %d \n", numberOfDest);

        for (int k = 0; k < numberOfDest; k++) {
            std::getline(ss, buf, ' ');
            r->dest.push_back(getNode(listNodes, (char *) buf.c_str()));
        }

        listRequest->push_back(r);
    }
    in.close();
}

void ResquestGeneration::saveFile(char * file) {

    FILE * pFile;

    pFile = fopen(file, "w");

    fprintf(pFile, "FailureOrder ");
    for (int i = 0; i < failureOrder.size(); i++) {
        fprintf(pFile, "%d ", failureOrder[i]->id);
    }
    fprintf(pFile, "\n");
    fflush(pFile);

    for (std::list<Request*>::iterator it = listRequest->begin(); it != listRequest->end(); it++) {

        fprintf(pFile, "%d ", (*it)->id);

        for (int k = 0; k < NUMBER_OF_SLOTS; k++) {
            fprintf(pFile, "%f ", (*it)->bw[k]);
        }

        fprintf(pFile, " %f %d %s %d ", (*it)->reliability, (*it)->duration, (*it)->source->name, (int) (*it)->dest.size());

        for (std::list<node *>::iterator d = (*it)->dest.begin(); d != (*it)->dest.end(); d++) {
            fprintf(pFile, "%s ", (*d)->name);
        }
        fprintf(pFile, "\n");
        fflush(pFile);
    }
    fclose(pFile);

}

node* ResquestGeneration::getNode(std::list<node*> list, int n) {
    int i = 0;
    for (std::list<node *>::iterator it = list.begin(); it != list.end(); it++) {
        if (i == n) {
            return *(it);
        }
        i++;
    }
}

node* ResquestGeneration::getNode(std::list<node*> list, char* name) {
    for (std::list<node *>::iterator it = list.begin(); it != list.end(); it++) {
        //        printf("node by name: %s (%d) == %s ??? \n", (*it)->name, (*it)->id, name);
        if (strcmp(name, (*it)->name) == 0) {
            return (*it);
        }
    }
    //    printf("\n-----------------\n");
}

networkComponent* ResquestGeneration::getComponent(topologyInfo* t, int id) {
    for (std::list<node *>::iterator it = t->nodes->begin(); it != t->nodes->end(); it++) {
        if ((*it)->id == id)
            return (*it);
    }
    for (std::list<linkInfo*>::iterator it = t->links->begin(); it != t->links->end(); it++) {
        if ((*it)->id == id)
            return (*it);
    }
}

void ResquestGeneration::generateRequestList(std::list<node*> listNodes, int numberDest, double minReliability, int meanBw, int n, int avgDuration) {
    /* initialize random seed: */
    srand(time(NULL));
    double totalBw = 0;

    for (int i = 1; i <= n; i++) {
        std::vector<int> v(listNodes.size(), 0);
        Request* r = new Request;

        std::list<node *> dest;
        std::vector<double> listBw;
        for (int k = 0; k < NUMBER_OF_SLOTS; k++) {
            double bw = ((int) exponential((double) meanBw)) + 1;
            r->bw.push_back(bw);
            listBw.push_back(bw);
        }


        double rel;
        if (fixedRel == 1) {
            rel = minReliability;
        } else {
            rel = (rand() % (int) (((0.99 - minReliability)*100.0)) + (minReliability * 100.0)) / 100;
        }

        int nDest = rand() % numberDest + 1;

        int posSource = rand() % listNodes.size();

        int duration;
        if (avgDuration != 0) {
            duration = ((int) exponential((double) avgDuration)) + 1;
            if (duration < avgDuration * 0.2) {
                duration = avgDuration * 0.2;
            }
            if (duration > avgDuration * 1.8) {
                duration = avgDuration * 1.8;
            }

        } else {
            duration = -1;
        }
        node * source = new node;
        *source = *(getNode(listNodes, posSource));
        v[posSource] = 1;

        for (int j = 0; j < nDest; j++) {
            int posDest = rand() % listNodes.size();
            if (v[posDest] == 0) {
                v[posDest] = 1;
                node * d = new node;
                (*d) = *(getNode(listNodes, posDest));
                dest.push_back(d);
            } else {
                j--;
            }
        }

        printf("id %d : bw [ ", i);
        for (int k = 0; k < NUMBER_OF_SLOTS; k++) {
            printf("%f ", listBw[k]);
        }
        printf("] rel %f dur %d source %s dest [ ", rel, duration, source->name);
        for (std::list<node *>::iterator it = dest.begin(); it != dest.end(); it++) {
            printf("%s ", (*it)->name);
        }
        printf("]\n");

        totalBw += listBw[0];

        //r->bw = bw;
        r->reliability = rel;
        r->id = i;
        r->source = source;
        r->dest = dest;
        r->duration = duration;

        listRequest->push_back(r);
    }

    printf("\nTotal BW: %f\n", totalBw);

}

void ResquestGeneration::generateBwListRequestList(std::list<node*> listNodes, int numberDest, double minReliability, int n, char * fileBw, char * fileD) {
    /* initialize random seed: */
    srand(time(NULL));
    double totalBw = 0;

    std::ifstream in(fileBw);
    std::ifstream inDest(fileD);
    FILE * pFile;
    if (!inDest.good())
        pFile = fopen(fileD, "w+");

    for (int i = 1; i <= n; i++) {

        std::vector<int> v(listNodes.size(), 0);
        Request* r = new Request;
        std::string line;
        getline(in, line);
        std::stringstream ss(line);
        std::string buf;
        std::list<node *> dest;
        std::vector<double> listBw;
        for (int k = 0; k < NUMBER_OF_SLOTS; k++) {
            std::getline(ss, buf, '\t');
            double bw = atoi(buf.c_str());
            r->bw.push_back(bw);
            listBw.push_back(bw);
        }

        double rel;
        if (fixedRel == 1) {
            rel = minReliability;
        } else {
            rel = (rand() % (int) (((0.99 - minReliability)*100.0)) + (minReliability * 100.0)) / 100;
        }

        int nDest = rand() % numberDest + 1;
        int duration = -1;
        node * source = new node;

        if (inDest.good()) {

            std::string lineDest;
            getline(inDest, lineDest);
            std::stringstream ssDest(lineDest);
            std::string buf;
            //std::getline(ss, buf, '\t');

            std::getline(ssDest, buf, '\t');
            //printf("%s \n", (char *) buf.c_str());
            *source = *(getNode(listNodes, (char *) (buf.c_str())));
            for (int j = 0; j < nDest; j++) {
                std::getline(ssDest, buf, '\t');
                node * d = new node;
                (*d) = *(getNode(listNodes, (char *) (buf.c_str())));
                dest.push_back(d);
                printf("%s --\n", (char *) buf.c_str());
                printf("%s --\n", (char *) d->name);
            }

        } else {
            int posSource = rand() % listNodes.size();
            *source = *(getNode(listNodes, posSource));
            v[posSource] = 1;
            fprintf(pFile, "%s\t", source->name);
            for (int j = 0; j < nDest; j++) {
                int posDest = rand() % listNodes.size();
                if (v[posDest] == 0) {
                    v[posDest] = 1;
                    node * d = new node;
                    (*d) = *(getNode(listNodes, posDest));
                    dest.push_back(d);
                    printf("--- %d\n", posDest);
                    fprintf(pFile, "%s\t", d->name);
                    printf("s----------\n");
                    fflush(pFile);
                } else {
                    j--;
                }
            }
            fprintf(pFile, "\n");
            fflush(pFile);
        }

        printf("id %d : bw [ ", i);
        for (int k = 0; k < NUMBER_OF_SLOTS; k++) {
            printf("%f ", listBw[k]);
        }
        printf("] rel %f dur %d source %s dest [ ", rel, duration, source->name);
        for (std::list<node *>::iterator it = dest.begin(); it != dest.end(); it++) {
            printf("%s ", (*it)->name);
        }
        printf("]\n");

        totalBw += listBw[0];

        //r->bw = bw;
        r->reliability = rel;
        r->id = i;
        r->source = source;
        r->dest = dest;
        r->duration = duration;

        listRequest->push_back(r);
    }
    if (!inDest.good())
        fclose(pFile);

    printf("\nTotal BW: %f\n", totalBw);

}