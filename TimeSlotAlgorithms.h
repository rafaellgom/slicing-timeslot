/* 
 * File:   VnForEachSlotAlgorithm.h
 * Author: rafaellgom
 *
 * Created on August 15, 2016, 2:27 PM
 */

#ifndef VNFOREACHSLOTALGORITHM_H
#define	VNFOREACHSLOTALGORITHM_H

#include "reliability.h"
#include "topologyManager.h"
#include "basicAlgoithms.h"


#define MAX_BW_REQUEST      0
#define SLICE_FOR_EACH_SLOT    1
#define SLICE_PREVIOUS_FORCE 2
#define SLICE_ENCOURAGE 3
#define SLICE_COMPARE_BW 4
#define SLICE_COMPARE_ENERGY 5
#define SLICE_COMPARE_SATURATION 6

#define NO_RELIABILITY  0
#define DISJOINT_RELIABILITY  1

class TimeSlotAlgorithms : public basicAlgoithms {
public:
    TimeSlotAlgorithms();
    TimeSlotAlgorithms(std::list<linkInfo*> *, std::list<node*> *);//,int, int,int*);
    virtual ~TimeSlotAlgorithms();

    std::list<topologyInfo*>* sliceForEachSlot(node*, std::list<node*>*, double, std::vector<double>, std::list<topologyInfo*>* dayAllocation, int* migration,int);
    std::list<topologyInfo*>* maxBwRequest(node*, std::list<node*>*, double, std::vector<double>, std::list<topologyInfo*>* dayAllocation, int* migration);
    std::list<topologyInfo*>* defineTopology(node*, std::list<node*>*, double, std::vector<double>, int* migration, int,int);
    std::list<topologyInfo*>* slicePreviousForce(node* , std::list<node*>* , double , std::vector<double> , std::list<topologyInfo*>* dayAllocation, int* migration);
    std::list<topologyInfo*>* sliceEncourage(node* , std::list<node*>* , double , std::vector<double> , std::list<topologyInfo*>* dayAllocation, int* migration,int);
    std::list<topologyInfo*>* sliceCompareBwUsage(node* , std::list<node*>* , double , std::vector<double> , std::list<topologyInfo*>* dayAllocation, int* migration);
    std::list<topologyInfo*>* sliceCompareEnergy(node* , std::list<node*>* , double , std::vector<double> , std::list<topologyInfo*>* dayAllocation, int* migration);
    std::list<topologyInfo*>* sliceCompareSaturation(node* , std::list<node*>* , double , std::vector<double> , std::list<topologyInfo*>* dayAllocation, int* migration);
    void topology_update(topologyInfo* past_topology, int TIME, std::vector<double> bwRequest);
    bool topology_ok(topologyInfo* past_topology, int TIME, std::vector<double> bwRequest);
    double links_total_energy(topologyInfo* past_topology, int TIME);
    double links_total_saturation(topologyInfo* past_topology, int TIME);
    bool same_topology(topologyInfo* past_topology, topologyInfo* new_topology);
    topologyInfo * copy_topology(topologyInfo* past_topology, int TIME, std::vector<double> bwRequest);
    void updateLinkList(std::map<node*, pathInfo*>* paths, int time, double percent);
    int timeSlotAlgorithm;
    int disjointAlgorithm;
};

#endif	/* VNFOREACHSLOTALGORITHM_H */

