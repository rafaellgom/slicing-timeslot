/* 
 * File:   main.cpp
 * Author: rafaellgom
 *
 * Created on July 16, 2012, 3:41 PM
 */

#include <ctime>
#include <time.h>
#include <stdio.h>
#include <iostream>     // std::cout, std::ios
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>
#include "reliability.h"
#include "topologyManager.h"
#include "ResquestGeneration.h"
#include "RegionRisk.h"
#include "TimeSlotAlgorithms.h"

using namespace std;

/*
 * Exemplo:
 ./slicing-timeslot --topology GEANT.xml --requests GEANT.requests --objMetric 6 --reliabilityApproach 1 --numReqGen 100 --meanBw 50  --maxDest 2 --minRel 0.9 --duration 0 --failureCheck 1  --fixedRel 1 --timeSlotAlgorithm 0
 */
std::list<RegionFailure>* failureList();

int main(int argc, char * argv[]) {

    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " <TopologyFilePath> <RequestsFilePath>" << std::endl;
        return 1;
    }
    char *topologyFile;
    char *requestListFile;
    char *bwListFile;
    char *destListFile;
    int objMetric = 0;
    int numReqGen = 0;
    double minRel = 0;
    int maxDest = 1;
    int meanBw = 0;
    int fails = 1;
    int avgDuration = 0;
    double weightEnergy = 0;
    int failureCheck = 1;
    int dynamicAdjustment = 0;
    int maxDisjoint = 1;
    int reliabilityApproach = 0;
    int timeSlotAlgorithm = 0;
    int fixedRel = 0;

    // Check arguments
    for (int i = 1; i < argc; i++) {
        if (i + 1 != argc)
            if (strcmp(argv[i], "--bwList") == 0) {
                bwListFile = (char *) argv[i + 1];
                i++;
            } else if (strcmp(argv[i], "--destList") == 0) {
                destListFile = (char *) argv[i + 1];
                i++;
            } else if (strcmp(argv[i], "--requests") == 0) {
                requestListFile = (char *) argv[i + 1];
                i++;
            } else if (strcmp(argv[i], "--topology") == 0) {
                topologyFile = (char *) argv[i + 1];
                i++;
            } else if (strcmp(argv[i], "--objMetric") == 0) {
                objMetric = atoi(argv[i + 1]);
                i++;
            } else if (strcmp(argv[i], "--reliabilityApproach") == 0) {
                reliabilityApproach = atoi(argv[i + 1]);
                i++;
            } else if (strcmp(argv[i], "--numReqGen") == 0) {
                numReqGen = atoi(argv[i + 1]);
                i++;
            } else if (strcmp(argv[i], "--meanBw") == 0) {
                meanBw = atoi(argv[i + 1]);
                i++;
            } else if (strcmp(argv[i], "--maxDest") == 0) {
                maxDest = atoi(argv[i + 1]);
                i++;
            } else if (strcmp(argv[i], "--fails") == 0) {
                fails = atoi(argv[i + 1]);
                i++;
            } else if (strcmp(argv[i], "--minRel") == 0) {
                minRel = atof(argv[i + 1]);
                i++;
            } else if (strcmp(argv[i], "--wEnergy") == 0) {
                weightEnergy = atof(argv[i + 1]);
                i++;
            } else if (strcmp(argv[i], "--duration") == 0) {
                avgDuration = atoi(argv[i + 1]);
                i++;
            } else if (strcmp(argv[i], "--failureCheck") == 0) {
                failureCheck = atoi(argv[i + 1]);
                i++;
                //            } else if (strcmp(argv[i], "--dynamicAdjustment") == 0) {
                //                dynamicAdjustment = atoi(argv[i + 1]);
                //                i++;
            } else if (strcmp(argv[i], "--fixedRel") == 0) {
                fixedRel = atoi(argv[i + 1]);
                i++;
                //            } else if (strcmp(argv[i], "--maxDisjoint") == 0) {
                //                maxDisjoint = atoi(argv[i + 1]);
                //                i++;
            } else if (strcmp(argv[i], "--timeSlotAlgorithm") == 0) {
                timeSlotAlgorithm = atoi(argv[i + 1]);
                i++;
            } else {
                printf("invalid argument %d, please try again.\n", i);
                exit(0);
            }

    }

    //read the topology file, usually topology.xml
    std::ifstream topFile(topologyFile);
    topologyManager* topology;
    if (topFile.good()) {
        topology = new topologyManager(topologyFile);
    } else {
        printf("Invalid topology file!\n");
        exit(0);
    }

    //set energy weight
    topology->setWeightEnergy(weightEnergy);

    //Analyze the set of requests
    ResquestGeneration* req = new ResquestGeneration(fixedRel);
    std::ifstream reqFile(requestListFile);
    //Check if a set of req exist, then it is loaded, otherwise a random set is generated according to arguments
    if (reqFile.good()) {
        req->loadFile(requestListFile, *(topology->getOriginalTopology()->nodes), topology->getOriginalTopology());
    } else {
        std::ifstream bwFile(bwListFile);
        //std::ifstream destFile(destListFile);
        if (bwFile.good()) {
            req->generateBwListRequestList(*(topology->getOriginalTopology()->nodes), maxDest, minRel, numReqGen, bwListFile,destListFile);
        } else {
            req->generateRequestList(*(topology->getOriginalTopology()->nodes), maxDest, minRel, meanBw, numReqGen, avgDuration);
        }
        req->generateFailureOrder(topology->getOriginalTopology());
        req->saveFile(requestListFile);

    }

    //the requests are loaded
    std::list<Request*>* listRequest = new std::list<Request*>();
    std::list<Request*>* activeRequest = new std::list<Request*>();
    *listRequest = req->getResquestList();
    std::list<RegionFailure>* dList;

    if (failureCheck == 1) {
        dList = failureList();
    }

    int reqSolved = 0, reqDeniedRel = 0, reqDeniedBw = 0, reqStatusWindow[10];
    double time = 0, energySpent = 0, enXact = 0;

    for (int i = 0; i < 10; i++) {
        reqStatusWindow[i] = 1;
    }

    std::string outText;

    //prints the information about the request and the status of the network resources
    printf("RequestID\t%d\t", 0);
    printf("ReqReliab\t%f\t", 0);
    printf("ReqBW\t%f\t", 0.0); //bw);
    printf("Percent\t%f\t", 0);
    printf("Reliability\t%f\t", 0);
    //printf("Bw\t%f\t", topology->averageAvailableBw());
    topology->averageAvailableBw();
    //                //printf("Saturated\t%f\t", topology->numberLowBw(250));
    topology->numberLowBw(250);
    topology->energyEfficiency(activeRequest);
    printf("%s\n\n", outText.c_str());

    RegionRisk* regionRisk = new RegionRisk(req->getFailureOrder(), topology->getOriginalTopology());
    ////    printf("RequestID\t%d\tReqReliab\t%d\tReqBW\t%d\tPercent\t%d\tReliability\t%d\tBw\t%f\tTime\t%d\tSaturated\t%f\tEnergy\t%d\tActive\t%d\t...\n\n",
    ////            0, 0, 0, 0, 0, topology->averageAvailableBw(), 0, topology->numberLowBw(100), 0, (int) activeRequest->size());

    //the requests are solved (or not) sequentially 
    for (std::list<Request*>::iterator it = listRequest->begin(); it != listRequest->end(); it++) {
        ////
        //the information from the request is loaded: source node, dest nodes, bw requested and desired reliability.
        double requestedReliability = (*it)->reliability;
        std::vector<double> bw = (*it)->bw;
        std::list<node *> * destNodes = new std::list<node *>();
        *destNodes = (*it)->dest;
        node * source = (*it)->source;

        //variables to control the request analyzes 
        bool found = false;
        double r = 0, best = 1000, best_r = 0;
        topologyInfo* bestTopology = new topologyInfo();
        double percent = 0, best_percent = 0;
        int numberOfMigration = 0;

        //        printf("Req %d: name %s : ", (*it)->id, source->name);
        //        for(std::list<node *>::iterator itDest = destNodes->begin(); itDest != destNodes->end(); itDest++ ){
        //            printf("%s \t", (*itDest)->name);
        //        }
        //        printf("\n");

        std::list<topologyInfo*>* t; // = new topologyInfo();
        t = topology->generateSlices(source, destNodes, requestedReliability, bw, &numberOfMigration,
                objMetric, reliabilityApproach, timeSlotAlgorithm);

        if (t->size() == NUMBER_OF_SLOTS) {
            topology->updateTopologyResources(t);
            (*it)->topologyAllocated = t;
            activeRequest->push_back(*it);
            reqSolved++;
            outText = "Success";

            if (failureCheck == 1) {
                for (std::list<RegionFailure>::iterator it = dList->begin(); it != dList->end(); it++) {
                    double avgComm = 0;
                    for (std::list<topologyInfo*>::iterator topo = t->begin(); topo != t->end(); topo++) {
                        avgComm += regionRisk->commUnderDisaster(it->region, *topo, it->percent,
                                destNodes, source);
                    }
                    (*it).communication += avgComm / t->size();
                }
            }

        } else {
            numberOfMigration = -1;
            outText = "Fail";
            if (t->size() == 0)
                reqDeniedRel++;
            else
                reqDeniedBw++;
        }

        //prints the information about the request and the status of the network resources
        printf("RequestID\t%d\t", (*it)->id);
        printf("ReqReliab\t%f\t", requestedReliability);
        printf("ReqBW\t%f\t", 0.0); //bw);
        printf("Percent\t%f\t", best_percent);
        printf("Reliability\t%f\t", best_r);
        //printf("Bw\t%f\t", topology->averageAvailableBw());
        topology->averageAvailableBw();
        //                //printf("Saturated\t%f\t", topology->numberLowBw(250));
        topology->numberLowBw(0.2*(*topology->getOriginalTopology()->links->begin())->originalBw);
        topology->energyEfficiency(activeRequest);
        printf("Migration\t%d\t", numberOfMigration);
        printf("%s\n\n", outText.c_str());

        for (std::list<Request*>::iterator req = activeRequest->begin(); req != activeRequest->end(); req++) {
            if ((*req)->duration > 0) {
                (*req)->duration--;
                if ((*req)->duration == 0) {
                    topology->releaseTopologyResources((*req)->topologyAllocated);
                    delete (*req)->topologyAllocated;
                    activeRequest->erase(req);
                    req--;
                }
            }

        }

    }

    printf("\n------------------------------------------------------------------------\n");
    printf("Solved\t%d\tDenyRel\t%d\tDenyBw\t%d\tTime\t%f\tEE\t%f\tEff\t%f\n", reqSolved, reqDeniedRel, reqDeniedBw, time,
            energySpent / numReqGen, enXact / numReqGen);
    printf("\n");

    if (failureCheck == 1) {
        printf("FailureInfo");
        for (std::list<RegionFailure>::iterator it = dList->begin(); it != dList->end(); it++) {
            //printf("\t%s-%d\t%f\t", it->region, (int) (it->percent * 100), it->communication / (double) numReqGen);
            //printf("\t%d\%\t%f\t", (int) (it->percent * 100), it->communication / (double) numReqGen);
            if (reqSolved != 0) {
                printf("\t%d\%\t%f\t", (int) (it->percent * 100), it->communication); // / (double) reqSolved);
            } else {
                printf("\t-%d\%\t%f\t", (int) (it->percent * 100), 0);
            }
        }
        printf("\n");
    }

    return 0;
}

std::list<RegionFailure>* failureList() {
    std::list<RegionFailure>* list = new std::list<RegionFailure>();

    RegionFailure rd;

    rd.region = (char *) "0";
    rd.percent = 0.00;
    rd.communication = 0;
    list->push_back(rd);

    //    rd.region = (char *) "0";
    //    rd.percent = 0.02;
    //    rd.communication = 0;
    //    list->push_back(rd);
    //
    //    rd.region = (char *) "0";
    //    rd.percent = 0.03;
    //    rd.communication = 0;
    //    list->push_back(rd);
    //
    //    rd.region = (char *) "0";
    //    rd.percent = 0.04;
    //    rd.communication = 0;
    //    list->push_back(rd);

    rd.region = (char *) "0";
    rd.percent = 0.05;
    rd.communication = 0;
    list->push_back(rd);

    //    rd.region = (char *) "0";
    //    rd.percent = 0.1;
    //    rd.communication = 0;
    //    list->push_back(rd);

    rd.region = (char *) "0";
    rd.percent = 0.15;
    rd.communication = 0;
    list->push_back(rd);

    //    rd.region = (char *) "0";
    //    rd.percent = 0.2;
    //    rd.communication = 0;
    //    list->push_back(rd);

    rd.region = (char *) "0";
    rd.percent = 0.25;
    rd.communication = 0;
    list->push_back(rd);

    //    rd.region = (char *) "0";
    //    rd.percent = 0.3;
    //    rd.communication = 0;
    //    list->push_back(rd);

    //    rd.region = (char *) "0";
    //    rd.percent = 0.35;
    //    rd.communication = 0;
    //    list->push_back(rd);

    //    rd.region = (char *) "0";
    //    rd.percent = 0.4;
    //    rd.communication = 0;
    //    list->push_back(rd);

    //    rd.region = (char *) "0";
    //    rd.percent = 0.45;
    //    rd.communication = 0;
    //    list->push_back(rd);

    rd.region = (char *) "0";
    rd.percent = 0.5;
    rd.communication = 0;
    list->push_back(rd);

    //    rd.region = (char *) "0";
    //    rd.percent = 0.75;
    //    rd.communication = 0;
    //    list->push_back(rd);

    //    rd.region = (char *) "0";
    //    rd.percent = 1.0;
    //    rd.communication = 0;
    //    list->push_back(rd);

    //        rd.region = (char *) "E";
    //        rd.percent = 0.1;
    //        rd.communication = 0;
    //        list->push_back(rd);
    //
    //        rd.region = (char *) "E";
    //        rd.percent = 0.5;
    //        rd.communication = 0;
    //        list->push_back(rd);
    //
    //        rd.region = (char *) "E";
    //        rd.percent = 1;
    //        rd.communication = 0;
    //        list->push_back(rd);
    //
    //        rd.region = (char *) "F";
    //        rd.percent = 0.1;
    //        rd.communication = 0;
    //        list->push_back(rd);
    //
    //        rd.region = (char *) "F";
    //        rd.percent = 0.5;
    //        rd.communication = 0;
    //        list->push_back(rd);
    //
    //        rd.region = (char *) "F";
    //        rd.percent = 1;
    //        rd.communication = 0;
    //        list->push_back(rd);
    //
    //        rd.region = (char *) "H";
    //        rd.percent = 0.1;
    //        rd.communication = 0;
    //        list->push_back(rd);
    //
    //        rd.region = (char *) "H";
    //        rd.percent = 0.5;
    //        rd.communication = 0;
    //        list->push_back(rd);
    //
    //        rd.region = (char *) "H";
    //        rd.percent = 1;
    //        rd.communication = 0;
    //        list->push_back(rd);

    return list;
}