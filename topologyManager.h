/*
 * File:   TopologyManager.h
 * Author: rafaellgom
 *
 * Created on July 16, 2012, 3:45 PM
 */

#ifndef TOPOLOGYMANAGER_H
#define	TOPOLOGYMANAGER_H

#define MAXINT 9999999
#define MININT 0.00001

#include <vector>
#include <set>
#include <string.h>
#include <map>
#include <list>
#include <libxml2/libxml/xmlreader.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "reliability.h"
#include "ResquestGeneration.h"

#define SWITCH_CHASSI_ENERGY 50
#define SWITCH_LINECARD_ENERGY 40
#define LINK_1GBPS_ENERGY 1
#define LINK_100MBPS_ENERGY 0.5
#define LINK_10MBPS_ENERGY 0.4
#define LINK_ENERGY 82 // 2*(SWITCH_LINECARD_ENERGY + LINK_1GBPS_ENERGY) - 2 placas mais o cabo

#define LINK 0
#define SWITCHS 1
#define ROUTER 2

#define INCREMENT_FACTOR 0.25

//criteria shortest path
#define LOWEST_WEIGHTED_PATH 0
#define MAX_AVAL_BW 1
#define FEASIBLE_BW 8
#define BW_RISK_RATIO 3
#define DBW 4
#define ENERGY_AWARE 5
#define BW_ENERGY_MIX 6
#define DA_BW_ENERGY 7
#define BW_LOG 2

//algorithm disjoint path
#define NO_DISJ_ALG 0
#define RELI 1
#define BRAR 2
#define RDP 3
#define MERB 4
#define RKSP 5
#define MSPS 6

//day allocation algorithm
#define NUMBER_OF_SLOTS 24
#define UPPERBOUND 0
#define VN_EACH_SLOT 1

class interface {
public:
    char * name;
    char * mac;
    char * ip;

    bool operator<(const interface & A) const {
        return (
                //(strcmp(mac, A.mac) != 0) ||
                //(strcmp(name, A.name) != 0) ||
                (strcmp(mac, A.mac) != 0)
                );
    }
};

class networkComponent {
public:
    int id;
    char * name;
    int type;
    //componentType type;
    int active;
    double reliability;
    double energy;
    double regionRisk;
    char * region;
};

class networkState {
public:

    virtual ~networkState() {
    };
    int id;//long id;
    int comm;
    double prob;
    std::vector<networkComponent*> state;
    std::vector<int> operational;

    bool operator<(const networkState & A) const {
        return (
                (prob < A.prob)
                );

    }
};

class node : public networkComponent {
public:
    int type; //0 openflowswitch and 1 router/host

    virtual ~node() {
    };

    bool operator<(const node & A) const {
        return ((id != A.id));
    }

    bool operator==(const node & A) const {
        return ((id == A.id));
    }

    //std::list<interface> intfs;
    std::list<node*>* neighbors;
};

class linkInfo : public networkComponent {
public:
    node* from, *to;
    std::vector<double> weight;
    std::vector<double> bw;
    std::vector<double> allocatedBw;
    double originalBw;
    bool used;
    int hop;
    int timesUsed;

    bool operator<(const linkInfo & A) const {
        return (((to->id != A.to->id) || (from->id != A.from->id)));
    }

    bool operator==(const linkInfo & A) const {
        return (((to->id == A.to->id) && (from->id == A.from->id)));
    }
};

class pathInfo {
public:
    std::list<node*> nodesSequence;
    std::list<linkInfo*> linkSequence;
    double totalWeight;
    bool ok;
    int hops;
};

class topologyInfo {
public:
    topologyInfo();
    ~topologyInfo();
    std::list<linkInfo*> * links;
    std::list<node *> * nodes;

    double minProb;
    int numberUnreliable;
    std::vector<double> bwReq;
    double reliabilityReq;
    bool ok;
};
//******************************

class topologyManager {
public:
    topologyManager(char *, int, int,int);
    topologyManager(char *);
    virtual ~topologyManager();

    std::list<topologyInfo*>* generateSlices(node * r, std::list<node *> * destNodes, double percent, 
    std::vector<double> request, int*, int, int, int);

    //get the information of the original topology

    topologyInfo* getOriginalTopology() {
        double totalBw = 0;
        for (std::list<linkInfo*>::iterator it = linkList->begin(); it != linkList->end(); it++) {
            totalBw += (*it)->weight[0];
        }

        topologyInfo* topology = extractTopologyInfo(linkList);
        topology->minProb = (*linkList->begin())->from->reliability;
        topology->numberUnreliable = topology->links->size() / 2 + topology->nodes->size();
        return topology;
    }

    double getReliability(topologyInfo* topology, std::list<node*>* dest, node* root, int fails);

    std::list<node*>* getDestination(std::list<node*>* destNodes) {
        dest = new std::list<node*>();
        for (std::list<node *>::iterator i = destNodes->begin(); i != destNodes->end(); i++) {
            dest->push_back(*i); //getNodeInfoByName(*i));

        }
        return dest;
    }

    std::list<node*>* getDestination() {
        return dest;
    }

    node* getRoot(node * r) {
        root = r; //getNodeInfoByName(r);
        return root;
    }

    node* getRoot() {
        return root;
    }

    double min(double a, double b) {
        if (a < b)
            return a;
        else
            return b;
    }

    double max(double a, double b) {
        if (a > b)
            return a;
        else
            return b;
    }

    bool isConnected(topologyInfo* topology);

    void updateTopologyResources(std::list<topologyInfo*>* topologiesDay);

    void releaseTopologyResources(std::list<topologyInfo*>* topologyAllocated);

    bool fitsResources(topologyInfo* topology);

    double bwUsage(topologyInfo* topology);

    double bwUsage();

    double lowerBw(topologyInfo* topology);

    double bwImpact(topologyInfo* t) {
        double sum = 0;
        for (int time = 0; time < NUMBER_OF_SLOTS; time++)
            for (std::list<linkInfo*>::iterator i = t->links->begin(); i != t->links->end(); i++) {
                for (std::list<linkInfo*>::iterator j = linkList->begin(); j != linkList->end(); j++) {
                    if ((*i)->id == (*j)->id) {
                        sum += (*i)->allocatedBw[time] / (*j)->bw[time];
                    }
                }
            }

        //printf("sum = %f\n",(sum / t->links->size()));
        return (sum / 2);
    };

    std::vector<double> averageAvailableBw() {
        std::vector<double> sum;
        printf("Bw-Available:\t");
        for (int time = 0; time < NUMBER_OF_SLOTS; time++) {
            sum.push_back(0);
            for (std::list<linkInfo*>::iterator it = linkList->begin(); it != linkList->end(); it++) {
                //printf("%f\n", (*it)->bw[time]);
                sum[time] += (*it)->bw[time];
            }
            sum[time] = sum[time] / linkList->size();
            printf("\t(%d)\t%f", time, sum[time]);
        }
        printf("\t");
        return sum;
    };

    std::vector<int> numberLowBw(double max) {
        std::vector<int> sum;
        printf("Saturated:\t");
        for (int time = 0; time < NUMBER_OF_SLOTS; time++) {
            sum.push_back(0);
            for (std::list<linkInfo*>::iterator it = linkList->begin(); it != linkList->end(); it++) {
                if ((*it)->bw[time] < max) {
                    sum[time]++;
                }
            }
            sum[time] = sum[time] / 2;
            printf("\t(%d)\t%d", time, sum[time]);
        }
        printf("\t");
        return sum;
    };

    std::vector<double> energyEfficiency(std::list<Request*> *activeRequest) {
        std::vector<double> sum;
        printf("Energy:\t");
        for (int time = 0; time < NUMBER_OF_SLOTS; time++) {
            sum.push_back(0);
            for (std::list<Request*>::iterator req = activeRequest->begin(); req != activeRequest->end(); req++) {
                sum[time] += (*req)->bw[time];
            }

            sum[time] /= energyConsumption(time);
            printf("\t(%d)\t%f", time, sum[time]);
        }
        printf("\t");
        return sum;
    }

    double energyConsumption(topologyInfo* t) {
        double sum = 0;
        for (int time = 0; time < NUMBER_OF_SLOTS; time++)
            for (std::list<linkInfo*>::iterator it = t->links->begin(); it != t->links->end(); it++) {
                //printf("%d -> %d \n", (*it)->from->id, (*it)->to->id);
                //if ((*it)->energy == 1)
                sum += getLinkEnergyConsumption((*it)->bw[time]) + SWITCH_LINECARD_ENERGY;
            }

        sum /= NUMBER_OF_SLOTS;

        for (std::list<node*>::iterator it = t->nodes->begin(); it != t->nodes->end(); it++) {
            //if ((*it)->energy == 1)
            sum += SWITCH_CHASSI_ENERGY;
        }
        return sum;
    }

    double energyConsumption() {
        double sum = 0;
        for (int time = 0; time < NUMBER_OF_SLOTS; time++)
            for (std::list<linkInfo*>::iterator it = linkList->begin(); it != linkList->end(); it++) {
                //printf("%d -> %d :: en %f\n", (*it)->from->id, (*it)->to->id, sum);
                if ((*it)->energy == 1)
                    sum += getLinkEnergyConsumption((*it)->bw[time]) + SWITCH_LINECARD_ENERGY;
            }
        sum = sum / (2 * NUMBER_OF_SLOTS);
        for (std::list<node*>::iterator it = nodesInfo->begin(); it != nodesInfo->end(); it++) {
            if ((*it)->energy == 1)
                sum += SWITCH_CHASSI_ENERGY;
        }
        return sum;
    }

    double energyConsumption(int time) {
        double sum = 0;
        for (std::list<linkInfo*>::iterator it = linkList->begin(); it != linkList->end(); it++) {
            //printf("%d -> %d :: en %f\n", (*it)->from->id, (*it)->to->id, sum);
            if ((*it)->energy == 1)
                sum += getLinkEnergyConsumption((*it)->bw[time]) + SWITCH_LINECARD_ENERGY;
        }
        sum = sum / (2);
        for (std::list<node*>::iterator it = nodesInfo->begin(); it != nodesInfo->end(); it++) {
            if ((*it)->energy == 1)
                sum += SWITCH_CHASSI_ENERGY;
        }
        return sum;
    }

    double maxEnergyConsumption() {
        double sum = 0;
        //printf("%d -> %d \n", (*it)->from->id, (*it)->to->id);
        sum += (LINK_1GBPS_ENERGY + SWITCH_LINECARD_ENERGY) * linkList->size();
        //printf("max %f \t", sum);
        sum += SWITCH_CHASSI_ENERGY * nodesInfo->size();
        //printf(" %f \n", sum);
        return sum;
    }
    //

    double getLinkEnergyConsumption(double bw) {
        double en = 0;

        if (bw <= 10) {
            en += LINK_10MBPS_ENERGY;
        } else if (bw <= 100) {
            en += LINK_100MBPS_ENERGY;
        } else {
            en += LINK_1GBPS_ENERGY;
        }

        //en+=SWITCH_LINECARD_ENERGY;

        //printf("en %f \t",en);

        return en;

    }

    double getLinkEnergyConsumptionNew(double bw) {
        double en = 0;

        if (bw <= 100) {
            en += 0.48;
        } else if (bw <= 100) {
            en += 0.9;
        } else {
            en += 1.7;
        }

        //en+=SWITCH_LINECARD_ENERGY;

        //printf("en %f \t",en);

        return en;

    }

    double setWeightEnergy(double w) {
        weightEnergy = w;
    }



    linkInfo* updateLinkWeight(linkInfo* l, int algorithm, std::vector<double> request);

private:

    node* nodeRegistred(node n) {
        for (std::list<node*>::iterator it = nodesInfo->begin(); it != nodesInfo->end(); it++) {
            if ((n.id == (*it)->id)) {
                if (strcmp(n.name, "empty") != 0) {
                    *(*it) = n;
                }
                return (*it);
            }
        }
        node * a = new node();
        (*a) = n;
        a->id = n.id;
        a->type = SWITCHS;
        a->neighbors = new std::list<node*>();
        nodesInfo->push_front(a);
        return (*(nodesInfo->begin()));
    };

    //xml read functions
    void readXml();
    void parseNode(xmlDocPtr doc, xmlNodePtr cur);
    void parseLink(xmlDocPtr doc, xmlNodePtr cur);

    node * getNodeById(int id) {
        for (std::list<node*>::iterator it = nodesInfo->begin(); it != nodesInfo->end(); it++) {
            if ((*it)->id == id) {
                return (*it);
            }
        }
    }

    node * getNodeByName(char * name) {
        for (std::list<node*>::iterator it = nodesInfo->begin(); it != nodesInfo->end(); it++) {
            if (strcmp((*it)->name, name) == 0) {
                return (*it);
            }
        }
    }

    node * getNodeInfoByName(char * name) {
        for (std::list<node*>::iterator it = nodesInfo->begin(); it != nodesInfo->end(); it++) {
            //printf("node by name: %s (%d) == %s ??? \n", (*it)->name, (*it)->id, name);
            if (strcmp((*it)->name, name) == 0) {
                return (*it);
            }
        }
    }

    //dijkstra
    void add_edge(node*, node*, double, double, double, char *);
    std::list<linkInfo*> * mergePaths(std::map<node*, pathInfo*>* first, std::map<node*, pathInfo*>* second, std::vector<double> bwReq);
    topologyInfo* extractTopologyInfo(std::list<linkInfo*> *);

    //******************************
    std::list<linkInfo*> * linkList;
    std::list<linkInfo*> *updatedlinkList;
    std::list<linkInfo*> * auxlinkList;
    std::map<node*, pathInfo*>* dijkstra(std::list<linkInfo*> * linkList, node * s, std::list<node*> *dest, std::vector<double> request);
    std::map<node*, double>* distance;
    std::list<node*>* dest;
    node* root;
    void updateLinkList(double);
    double calculateTopologyReliability(topologyInfo* t, std::list<node*>* d, node* r);

    //******************************

    std::list<node*> * nodesInfo;
    char * filename;
    double lowerBW;
//    int objMetric;
//    int timeSlotAlgorithm;
//    int reliabilityApproach;
    double weightEnergy;
};

#endif	/* TOPOLOGYMANAGER_H */
