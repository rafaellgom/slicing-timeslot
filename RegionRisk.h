/* 
 * File:   RegionRisk.h
 * Author: rafaellgom
 *
 * Created on November 18, 2014, 2:46 PM
 */

#ifndef REGIONRISK_H
#define	REGIONRISK_H

#include "topologyManager.h"
#include <algorithm>

class RegionRisk {
public:
    RegionRisk();
    RegionRisk(std::vector<networkComponent*>, topologyInfo*);
    virtual ~RegionRisk();
    
    //std::list<networkState*>*
    double commUnderDisaster(char *, topologyInfo*, double, std::list<node*>*, node *);
    double commUnderDisaster(bool);
    std::vector<networkComponent*> failuresGeneration(char * regionName, topologyInfo* t, std::list<node*>* d);
    int numberOfComponentsInRegion(char * regionName, topologyInfo* t);
    bool isDestination(networkComponent* no, std::list<node*>* d);
private:
    std::vector<networkComponent*> failuresOrder;
    topologyInfo* infrastructure;
};

#endif	/* REGIONRISK_H */
