/* 
 * File:   basicAlgoithms.h
 * Author: rafaellgom
 *
 * Created on December 19, 2016, 9:49 AM
 */

#ifndef BASICALGOITHMS_H
#define	BASICALGOITHMS_H

#include <vector>
#include <map>
#include <list>
#include <stdio.h>
#include <stdlib.h>
#include "topologyManager.h"

class basicAlgoithms {
public:
    basicAlgoithms();
    basicAlgoithms(const basicAlgoithms& orig);
    virtual ~basicAlgoithms();

    std::list<topologyInfo*>* defineTopology(node*, std::list<node*>*, double, std::vector<double>);
    std::map<node*, pathInfo*>* shortestPath(node*, std::list<node*>*, double, int);
    std::map<node*, pathInfo*>* findPaths(node*, std::list<node*>*, std::map<node*, node*>*, int);
    std::list<linkInfo*> * mergePaths(std::map<node*, pathInfo*>* first, std::map<node*, pathInfo*>* second, std::vector<double> bwReq);
    topologyInfo* extractTopologyInfo(std::list<linkInfo*> *);
    std::list<linkInfo*>* extractLinks(std::map<node*, pathInfo*>* first);
    
    std::map<node*, double>* distance;
    std::list<linkInfo*> * linkList;
    std::list<node*> * nodesInfo;

};

#endif	/* BASICALGOITHMS_H */

