/* 
 * File:   VnForEachSlotAlgorithm.cpp
 * Author: rafaellgom
 * 
 * Created on August 15, 2016, 2:27 PM
 */

#include "TimeSlotAlgorithms.h"
#include "topologyManager.h"

using namespace std;

//test

TimeSlotAlgorithms::TimeSlotAlgorithms() {
}

TimeSlotAlgorithms::TimeSlotAlgorithms(std::list<linkInfo*> * l, std::list<node*> * n) {//, int timeSlotAlg, int disjAlgo, int* migration) {
    linkList = l;
    nodesInfo = n;
    distance = new std::map<node*, double>();
    //    timeSlotAlgorithm = timeSlotAlg;
    //    disjointAlgorithm = disjAlgo;
}

TimeSlotAlgorithms::~TimeSlotAlgorithms() {
}
//list<topologyInfo*>* myfunc(node* r, list<node*>* dest, double reliability, vector<double> bwRequest, list<topologyInfo*>* dayAllocation);

double getLinkEnergyConsumption(double bw) {
    double en = 0;

    if (bw <= 10) {
        en += LINK_10MBPS_ENERGY;
    } else if (bw <= 100) {
        en += LINK_100MBPS_ENERGY;
    } else {
        en += LINK_1GBPS_ENERGY;
    }

    return en;
}

double getLinkEnergyConsumptionNew(double bw) {
    double en = 0;

    if (bw <= 100) {
        en += 0.48;
    } else if (bw <= 100) {
        en += 0.9;
    } else {
        en += 1.7;
    }
    return en;
}

double TimeSlotAlgorithms::links_total_energy(topologyInfo* topology, int TIME) {
    double sum = 0;
    for (list <linkInfo*> ::iterator i = topology->links->begin(); i != topology->links->end(); i++) {
        sum += getLinkEnergyConsumptionNew((*i)->bw[TIME]);
    }
    return sum;
}

double TimeSlotAlgorithms::links_total_saturation(topologyInfo* topology, int TIME) {
    double sum = 0;
    for (list <linkInfo*> ::iterator i = topology->links->begin(); i != topology->links->end(); i++) {
        sum += ((*i)->bw[TIME] / (*i)->originalBw);
    }
    return sum / topology->links->size();
}

void TimeSlotAlgorithms::topology_update(topologyInfo* past_topology, int TIME, vector<double> bwRequest) {
    int c = 0;
    for (list <linkInfo*> ::iterator i = past_topology->links->begin(); i != past_topology->links->end(); i++)
        if ((*i)->bw[TIME] >= bwRequest[TIME])//&& (*i)->bw[TIME] > (*i)->originalBw*0.8 )
            for (std::list<linkInfo*>::iterator itLink = linkList->begin(); itLink != linkList->end(); itLink++) {
                if ((*itLink)->from->id == (*i)->from->id && (*itLink)->to->id == (*i)->to->id) {
                    c++;
                    (*itLink)->weight[TIME] = MININT;
                }
            }//printf("vai incentivar o slice %d : %d \n", past_topology->links->size(), c);
}

bool TimeSlotAlgorithms::same_topology(topologyInfo* past_topology, topologyInfo* new_topology) {
    for (list <linkInfo*> ::iterator i = past_topology->links->begin(); i != past_topology->links->end(); i++) {
        bool found = false;
        for (list <linkInfo*> ::iterator j = new_topology->links->begin(); j != new_topology->links->end(); j++) {
            if ((*i)->id == (*j)->id) {
                found = true;
                break;
            }
        }
        if (found == false) {
            return false;
        }
    }
    return true;
}

bool TimeSlotAlgorithms::topology_ok(topologyInfo* past_topology, int TIME, vector<double> bwRequest) {
    for (list <linkInfo*> ::iterator i = past_topology->links->begin(); i != past_topology->links->end(); i++)
        if ((*i)->bw[TIME] < bwRequest[TIME]) return false;
    return true;
}

topologyInfo * TimeSlotAlgorithms::copy_topology(topologyInfo* past_topology, int TIME, vector<double> bwRequest) {
    topologyInfo* past_topology_copy = (topologyInfo*) past_topology;
    list <linkInfo*>::iterator j = past_topology_copy->links->begin();
    for (list <linkInfo*>::iterator i = past_topology->links->begin(); i != past_topology->links->end(); ++i, j++) {
        (*j)->bw[TIME] = (*i)->bw[TIME] - bwRequest[TIME];
        (*j)->allocatedBw[TIME] = (*i)->allocatedBw[TIME] + bwRequest[TIME];
    }
    return past_topology_copy;
}

void TimeSlotAlgorithms::updateLinkList(std::map<node*, pathInfo*>* paths, int time, double percent) {

    std::list<linkInfo*> * final = extractLinks(paths);
    int numberUpdates = final->size() * percent;

    for (std::list<linkInfo*>::iterator i = final->begin(); i != final->end(); i++) {
        for (std::list<linkInfo*>::iterator j = linkList->begin(); j != linkList->end(); j++) {
            if ((((*i)->to->id == (*j)->to->id) && ((*i)->from->id == (*j)->from->id))
                    //|| (((*i)->to->id == (*j)->from->id) && ((*i)->from->id == (*j)->to->id))  
                    ) {
                //printf("link %d -> %d : antes %f \t",(*j)->from->id, (*j)->to->id, (*j)->weight[time]);
                if (numberUpdates > 0) {
                    (*j)->weight[time] = MAXINT / 100;
                } else {
                    (*j)->weight[time] = MININT;
                }
                numberUpdates--;
                //printf("depois %f \n",(*j)->weight[time]);
            }
        }
    }

}

////////////////////////////////////////////////////////////////////

std::list<topologyInfo*>* TimeSlotAlgorithms::defineTopology(node* root, std::list<node*>* dest, double reliability,
        std::vector<double> bwRequest, int* migration, int timeSlotAlgorithm, int relApproach) {

    std::list<topologyInfo*>* dayAllocation = new std::list<topologyInfo*>();

    switch (timeSlotAlgorithm) {
        case MAX_BW_REQUEST:
            maxBwRequest(root, dest, reliability, bwRequest, dayAllocation, migration);
            break;

        case SLICE_FOR_EACH_SLOT:
            sliceForEachSlot(root, dest, reliability, bwRequest, dayAllocation, migration, relApproach);
            break;

        case SLICE_PREVIOUS_FORCE:
            slicePreviousForce(root, dest, reliability, bwRequest, dayAllocation, migration);
            break;

        case SLICE_ENCOURAGE:
            sliceEncourage(root, dest, reliability, bwRequest, dayAllocation, migration, relApproach);
            break;

        case SLICE_COMPARE_BW:
            sliceCompareBwUsage(root, dest, reliability, bwRequest, dayAllocation, migration);
            break;

        case SLICE_COMPARE_ENERGY:
            sliceCompareEnergy(root, dest, reliability, bwRequest, dayAllocation, migration);
            break;

        case SLICE_COMPARE_SATURATION:
            sliceCompareSaturation(root, dest, reliability, bwRequest, dayAllocation, migration);
            break;

        default:
            break;
    }

    return dayAllocation;

}

std::list<topologyInfo*>* TimeSlotAlgorithms::sliceForEachSlot(node* r, std::list<node*>* dest, double reliability, std::vector<double> bwRequest, std::list<topologyInfo*>* dayAllocation, int* migration, int relApproach) {

    for (int time = 0; time < bwRequest.size(); time++) {
        std::map<node*, pathInfo*>* paths = shortestPath(r, dest, bwRequest[time], time);
        std::map<node*, pathInfo*>* Additionalpaths;

        bool foundSolution = true;
        for (std::map<node*, pathInfo*>::iterator it = paths->begin(); it != paths->end(); it++) {
            if ((*it).second->ok == false) {
                foundSolution = false;
                //printf("FALHOU time %d: bw!!! \n", time);
                if (time == 0) {//day allocation vazio representa falha de confiabilidade, entao coloca um soh pra caracterizar bw
                    dayAllocation->push_back(new topologyInfo());
                }
                break;
            }
        }

        if (foundSolution == true) {
            topologyInfo* t = extractTopologyInfo(mergePaths(paths, paths, bwRequest));
            Reliability *rel = new Reliability(t, dest, r); //falhas fixas em 1
            t->reliabilityReq = rel->finalReliab;
            delete rel;
            if (t->reliabilityReq >= reliability) {
                if (time > 0) {
                    if (same_topology(t, dayAllocation->back()) == false) {
                        (*migration)++;
                    }
                }
                dayAllocation->push_back(t);
            } else {
                //printf("FALHOU time %d: confiabilidade %f !!! \n", time, t->reliabilityReq);
                foundSolution = false;
                if (relApproach == 1) {
                    double percent = INCREMENT_FACTOR;
                    while (percent <= 1 && foundSolution == false) {
                        //printf("Testando redundancia %f\t", percent);
                        updateLinkList(paths, time, percent);
                        Additionalpaths = shortestPath(r, dest, bwRequest[time], time);
                        t = extractTopologyInfo(mergePaths(paths, Additionalpaths, bwRequest));
                        Reliability *rel = new Reliability(t, dest, r); //falhas fixas em 1
                        t->reliabilityReq = rel->finalReliab;
                        delete rel;
                        //printf("confiabilidade %f \n", t->reliabilityReq);
                        //int a;scanf("%d",&a);

                        if (t->reliabilityReq >= reliability) {
                            if (time > 0) {
                                if (same_topology(t, dayAllocation->back()) == false) {
                                    (*migration)++;
                                }
                            }
                            dayAllocation->push_back(t);
                            foundSolution = true;
                        } else {
                            percent += INCREMENT_FACTOR;
                        }
                    }

                }

                if (foundSolution == false) {
                    dayAllocation->clear();
                    break;
                } else {
                    //printf("Achou com redundancia\n--------------------\n");
                }

            }

            if (dayAllocation->size() == 0) {
                break;
            }

        } else {
            break;
        }

    }

    return dayAllocation;
}

std::list<topologyInfo*>* TimeSlotAlgorithms::maxBwRequest(node* r, std::list<node*>* dest, double reliability, std::vector<double> bwRequest, std::list<topologyInfo*>* dayAllocation, int* migration) {

    int max = 0;
    for (int i = 0; i < bwRequest.size(); i++) {
        if (bwRequest[i] > max) {
            max = bwRequest[i];
        }
    }

    std::vector<double> maxRequest;
    for (int i = 0; i < bwRequest.size(); i++) {
        maxRequest.push_back(max);
    }

    for (int time = 0; time < bwRequest.size(); time++) {
        std::map<node*, pathInfo*>* paths = shortestPath(r, dest, maxRequest[time], time);

        bool foundSolution = true;
        for (std::map<node*, pathInfo*>::iterator it = paths->begin(); it != paths->end(); it++) {
            if ((*it).second->ok == false) {
                foundSolution = false;
                break;
            }
        }

        if (foundSolution == true) {
            topologyInfo* t = extractTopologyInfo(mergePaths(paths, paths, maxRequest));
            //            Reliability *rel = new Reliability(t, dest, r, 1); //falhas fixas em 1
            //            t->reliabilityReq = rel->finalReliab;
            //            delete rel;
            //            if (t->reliabilityReq >= reliability) {
            if (time > 0)
                if (same_topology(t, dayAllocation->back()) == false) {
                    (*migration)++;
                }
            dayAllocation->push_back(t);
            //            } else {
            //                break;
            //            }
        } else {
            break;
        }

    }

    return dayAllocation;
}

list<topologyInfo*>* TimeSlotAlgorithms::slicePreviousForce(node* r, list<node*>* dest, double reliability, vector<double> bwRequest, list<topologyInfo*>* dayAllocation, int* migration) {
    int bw_request_size = bwRequest.size();

    //ultima topologia alocada

    //verificando se o bw nao alocado atende ao request
    for (int TIME = 0; TIME < bw_request_size; ++TIME) {

        bool flag_previous_ok = false;
        topologyInfo* past_topology;
        //-----------------------------------------

        //se ja existir topologias alocadas TIME > 0
        if (TIME > 0) {
            past_topology = (dayAllocation->back());

            if (topology_ok(past_topology, TIME, bwRequest) == true) {
                dayAllocation->push_back(copy_topology(past_topology, TIME, bwRequest));
                //printf("Reuse %d in \t%d\n", TIME-1, TIME);
                flag_previous_ok = true;
            }
        }

        if (flag_previous_ok == false) {

            std::map<node*, pathInfo*>* paths = shortestPath(r, dest, bwRequest[TIME], TIME);

            bool foundSolution = true;
            for (std::map<node*, pathInfo*>::iterator it = paths->begin(); it != paths->end(); it++) {
                if ((*it).second->ok == false) {
                    foundSolution = false;
                    //printf("FALHOU %d!!! \n", TIME);
                    break;
                }
            }

            if (foundSolution == true) {
                topologyInfo* t = extractTopologyInfo(mergePaths(paths, paths, bwRequest));
                //                Reliability *rel = new Reliability(t, dest, r, 1); //falhas fixas em 1
                //                t->reliabilityReq = rel->finalReliab;
                //                delete rel;
                //                if (t->reliabilityReq >= reliability) {
                if (TIME > 0) {
                    if (same_topology(t, past_topology) == false) {
                        (*migration)++;
                    }
                }
                dayAllocation->push_back(t);
                //                    //printf("New in\t\t%d\n", TIME);
                //                } else {
                //                    //printf("FALHOU: confiabilidade !!! \n\n\n\n\n\n\n");
                //                    break;
                //                }
            } else {
                break;
            }

        }//if : flag
    }
    return dayAllocation;
}

list<topologyInfo*>* TimeSlotAlgorithms::sliceEncourage(node* r, list<node*>* dest, double reliability, vector<double> bwRequest, list<topologyInfo*>* dayAllocation, int* migration, int relApproach) {
    int bw_request_size = bwRequest.size();

    for (int time = 0; time < bw_request_size; ++time) {

        if (time > 0) {
            topologyInfo* past_topology = (dayAllocation->back());
            topology_update(past_topology, time, bwRequest);
        }

        std::map<node*, pathInfo*>* paths = shortestPath(r, dest, bwRequest[time], time);
        std::map<node*, pathInfo*>* Additionalpaths;

        bool foundSolution = true;
        for (std::map<node*, pathInfo*>::iterator it = paths->begin(); it != paths->end(); it++) {
            if ((*it).second->ok == false) {
                foundSolution = false;
                printf("FALHOU %d: bw!!! \n", time);
                if (time == 0) {//day allocation vazio representa falha de confiabilidade, entao coloca um soh pra caracterizar bw
                    dayAllocation->push_back(new topologyInfo());
                }
                break;
            }
        }

        if (foundSolution == true) {
            topologyInfo* t = extractTopologyInfo(mergePaths(paths, paths, bwRequest));
            Reliability *rel = new Reliability(t, dest, r); //falhas fixas em 1
            t->reliabilityReq = rel->finalReliab;
            delete rel;
            if (t->reliabilityReq >= reliability) {
                if (time > 0)
                    if (same_topology(t, dayAllocation->back()) == false) {
                        (*migration)++;
                    }
                dayAllocation->push_back(t);
            } else {
                //printf("FALHOU time %d: confiabilidade %f !!! \n", time, t->reliabilityReq);
                foundSolution = false;
                if (relApproach == 1) {
                    double percent = INCREMENT_FACTOR;
                    while (percent <= 1 && foundSolution == false) {
                        //printf("Testando redundancia %f\t", percent);
                        updateLinkList(paths, time, percent);
                        Additionalpaths = shortestPath(r, dest, bwRequest[time], time);
                        t = extractTopologyInfo(mergePaths(paths, Additionalpaths, bwRequest));
                        Reliability *rel = new Reliability(t, dest, r); //falhas fixas em 1
                        t->reliabilityReq = rel->finalReliab;
                        delete rel;
                        //printf("confiabilidade %f \n", t->reliabilityReq);
                        //int a;scanf("%d",&a);

                        if (t->reliabilityReq >= reliability) {
                            if (time > 0) {
                                if (same_topology(t, dayAllocation->back()) == false) {
                                    (*migration)++;
                                }
                            }
                            dayAllocation->push_back(t);
                            foundSolution = true;
                        } else {
                            percent += INCREMENT_FACTOR;
                        }
                    }

                }

                if (foundSolution == false) {
                    dayAllocation->clear();
                    break;
                } else {
                    //printf("Achou com redundancia\n--------------------\n");
                }

            }

            if (dayAllocation->size() == 0) {
                break;
            }
        } else {
            break;
        }

    }
    return dayAllocation;
}

list<topologyInfo*>* TimeSlotAlgorithms::sliceCompareBwUsage(node* r, list<node*>* dest, double reliability, vector<double> bwRequest, list<topologyInfo*>* dayAllocation, int* migration) {
    int bw_request_size = bwRequest.size();

    //ultima topologia alocada

    //verificando se o bw nao alocado atende ao request
    for (int TIME = 0; TIME < bw_request_size; ++TIME) {

        topologyInfo *t, *past_topology;
        bool flag_previous_ok = false, flag_new_ok = false;

        //-----------------------------------------

        //se ja existir topologias alocadas TIME > 0
        if (TIME > 0) {
            past_topology = (dayAllocation->back());

            if (topology_ok(past_topology, TIME, bwRequest) == true) {
                //dayAllocation->push_back(gerando_copia_topologia(past_topology, TIME, bwRequest));
                //printf("Reuse %d in \t%d\n", TIME-1, TIME);
                flag_previous_ok = true;
            }
        }



        std::map<node*, pathInfo*>* paths = shortestPath(r, dest, bwRequest[TIME], TIME);

        bool foundSolution = true;
        for (std::map<node*, pathInfo*>::iterator it = paths->begin(); it != paths->end(); it++) {
            if ((*it).second->ok == false) {
                foundSolution = false;
                //printf("FALHOU %d!!! \n", TIME);
                break;
            }
        }

        if (foundSolution == true) {
            t = extractTopologyInfo(mergePaths(paths, paths, bwRequest));
            //            Reliability *rel = new Reliability(t, dest, r, 1); //falhas fixas em 1
            //            t->reliabilityReq = rel->finalReliab;
            //            delete rel;
            //            if (t->reliabilityReq >= reliability) {
            flag_new_ok = true;
            //                //dayAllocation->push_back(t);
            //                //printf("New in\t\t%d\n", TIME);
            //            } else {
            //                //printf("FALHOU: confiabilidade !!! \n\n\n\n\n\n\n");
            //                break;
            //            }
        } else {
            break;
        }

        if (flag_new_ok && flag_previous_ok == false) {
            if (flag_previous_ok == true) {
                dayAllocation->push_back(copy_topology(past_topology, TIME, bwRequest));
            } else {
                dayAllocation->push_back(t);
                (*migration)++;
            }
        } else {//compara qual a melhor
            //printf("%f %f ? \n", links_total_energy(past_topology, TIME), links_total_energy(t, TIME));
            if (past_topology->links->size() <= t->links->size()) {
                dayAllocation->push_back(copy_topology(past_topology, TIME, bwRequest));
            } else {
                dayAllocation->push_back(t);
                (*migration)++;
            }
        }
    }
    return dayAllocation;
}

list<topologyInfo*>* TimeSlotAlgorithms::sliceCompareEnergy(node* r, list<node*>* dest, double reliability, vector<double> bwRequest, list<topologyInfo*>* dayAllocation, int* migration) {
    int bw_request_size = bwRequest.size();

    //ultima topologia alocada

    //verificando se o bw nao alocado atende ao request
    for (int TIME = 0; TIME < bw_request_size; ++TIME) {

        topologyInfo *t, *past_topology;
        bool flag_previous_ok = false, flag_new_ok = false;

        //-----------------------------------------

        //se ja existir topologias alocadas TIME > 0
        if (TIME > 0) {
            past_topology = (dayAllocation->back());

            if (topology_ok(past_topology, TIME, bwRequest) == true) {
                //dayAllocation->push_back(gerando_copia_topologia(past_topology, TIME, bwRequest));
                //printf("Reuse %d in \t%d\n", TIME-1, TIME);
                flag_previous_ok = true;
            }
        }



        std::map<node*, pathInfo*>* paths = shortestPath(r, dest, bwRequest[TIME], TIME);

        bool foundSolution = true;
        for (std::map<node*, pathInfo*>::iterator it = paths->begin(); it != paths->end(); it++) {
            if ((*it).second->ok == false) {
                foundSolution = false;
                //printf("FALHOU %d!!! \n", TIME);
                break;
            }
        }

        if (foundSolution == true) {
            t = extractTopologyInfo(mergePaths(paths, paths, bwRequest));
            //            Reliability *rel = new Reliability(t, dest, r, 1); //falhas fixas em 1
            //            t->reliabilityReq = rel->finalReliab;
            //            delete rel;
            //            if (t->reliabilityReq >= reliability) {
            flag_new_ok = true;
            //                //dayAllocation->push_back(t);
            //                //printf("New in\t\t%d\n", TIME);
            //            } else {
            //                //printf("FALHOU: confiabilidade !!! \n\n\n\n\n\n\n");
            //                break;
            //            }
        } else {
            break;
        }

        if (flag_new_ok && flag_previous_ok == false) {
            if (flag_previous_ok == true) {
                dayAllocation->push_back(copy_topology(past_topology, TIME, bwRequest));
            } else {
                dayAllocation->push_back(t);
                (*migration)++;
            }
        } else {//compara qual a melhor            
            if (links_total_energy(past_topology, TIME) <= links_total_energy(t, TIME)) {
                dayAllocation->push_back(copy_topology(past_topology, TIME, bwRequest));
            } else {
                dayAllocation->push_back(t);
                (*migration)++;
            }
        }
    }
    return dayAllocation;
}

list<topologyInfo*>* TimeSlotAlgorithms::sliceCompareSaturation(node* r, list<node*>* dest, double reliability, vector<double> bwRequest, list<topologyInfo*>* dayAllocation, int* migration) {
    int bw_request_size = bwRequest.size();

    //verificando se o bw nao alocado atende ao request
    for (int TIME = 0; TIME < bw_request_size; ++TIME) {

        topologyInfo *t, *past_topology;
        bool flag_previous_ok = false, flag_new_ok = false;

        //se ja existir topologias alocadas TIME > 0
        if (TIME > 0) {
            past_topology = (dayAllocation->back());

            if (topology_ok(past_topology, TIME, bwRequest) == true) {
                //dayAllocation->push_back(gerando_copia_topologia(past_topology, TIME, bwRequest));
                //printf("Reuse %d in \t%d\n", TIME-1, TIME);
                flag_previous_ok = true;
            }
        }

        std::map<node*, pathInfo*>* paths = shortestPath(r, dest, bwRequest[TIME], TIME);
        bool foundSolution = true;
        for (std::map<node*, pathInfo*>::iterator it = paths->begin(); it != paths->end(); it++) {
            if ((*it).second->ok == false) {
                foundSolution = false;
                //printf("FALHOU %d!!! \n", TIME);
                break;
            }
        }

        if (foundSolution == true) {
            t = extractTopologyInfo(mergePaths(paths, paths, bwRequest));
            //            Reliability *rel = new Reliability(t, dest, r, 1); //falhas fixas em 1
            //            t->reliabilityReq = rel->finalReliab;
            //            delete rel;
            //            if (t->reliabilityReq >= reliability) {
            flag_new_ok = true;
            //                //dayAllocation->push_back(t);
            //                //printf("New in\t\t%d\n", TIME);
            //            } else {
            //                //printf("FALHOU: confiabilidade !!! \n\n\n\n\n\n\n");
            //                break;
            //            }
        } else {
            break;
        }

        if (flag_new_ok && flag_previous_ok == false) {
            if (flag_previous_ok == true) {
                dayAllocation->push_back(copy_topology(past_topology, TIME, bwRequest));
            } else {
                dayAllocation->push_back(t);
                (*migration)++;
            }
        } else {//compara qual a melhor
            //printf("%f %f ? \n", links_total_saturation(past_topology, TIME), links_total_saturation(t, TIME));
            if (links_total_saturation(past_topology, TIME) <= links_total_saturation(t, TIME)) {
                dayAllocation->push_back(copy_topology(past_topology, TIME, bwRequest));
            } else {
                dayAllocation->push_back(t);
                (*migration)++;
            }
        }
    }
    return dayAllocation;
}
