/* 
 * File:   ResquestGeneration.h
 * Author: rafaellgom
 *
 * Created on July 11, 2013, 3:10 PM
 */

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <fstream>
#include <sstream>
#include <string>
#include <unistd.h>
#include <sys/time.h>
#include <algorithm>
#include "topologyManager.h"

#ifndef RESQUESTGENERATION_H
#define	RESQUESTGENERATION_H

struct RegionFailure {
    char * region;
    double percent;
    double communication;
};

struct Request {
    int id;
    std::list<node *> dest;
    node * source;
    std::vector<double> bw;
    double reliability;
    int duration;
    std::list<topologyInfo*>* topologyAllocated;
};

class ResquestGeneration {
public:
    ResquestGeneration(int);
    virtual ~ResquestGeneration();

    void generateFailureOrder(topologyInfo*);
    networkComponent* getComponent(topologyInfo* t, int id);
    void loadFile(char * file, std::list<node *> listNodes,topologyInfo*);
    void saveFile(char * file);
    void generateRequestList(std::list<node *> listNodes, int numberDest, double minReliability, int Bw, int n, int);
    void generateBwListRequestList(std::list<node *> listNodes, int numberDest, double minReliability, int n, char* ,char* file);
    node* getNode(std::list<node *> list, int n);
    node* getNode(std::list<node *> list, char* name);

    std::list<Request*> getResquestList() {
        return *listRequest;
    };
    
    std::vector<networkComponent*> getFailureOrder() {
        return failureOrder;
    }

////    double bwUsageActiveRequests(std::list<Request*> *activeRequest) {
////        double sum = 0, sum2 = 0;
////        for (std::list<Request*>::iterator req = activeRequest->begin(); req != activeRequest->end(); req++) {
////            sum += ((*req)->bw * (*req)->topologyAllocated->links->size());
////            sum2 += (*req)->bw;
////        }
////        return sum;
////    }
////    
//    double reqBwOfActiveRequests(std::list<Request*> *activeRequest) {
//        double sum = 0;
//        for (int time = 0; time < NUMBER_OF_SLOTS; time++)
//        for (std::list<Request*>::iterator req = activeRequest->begin(); req != activeRequest->end(); req++) {
//            sum += (*req)->bw[time];
//        }
//        return sum/NUMBER_OF_SLOTS;
//    }
    
private:
    std::list<Request*>* listRequest;
    char * filename;
    std::vector<networkComponent*> failureOrder;
    int fixedRel;
};

#endif	/* RESQUESTGENERATION_H */
