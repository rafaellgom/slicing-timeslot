/* 
 * File:   reliability.h
 * Author: rafaellgom
 *
 * Created on 29 de Setembro de 2012, 14:33
 */

#ifndef RELIABILITY_H
#define	RELIABILITY_H

#include <ctime>
#include <time.h>
#include <sys/time.h>
#include <vector>
#include <list>
#include <queue>
#include <math.h>
#include <iostream>

struct topologyInfo;
class networkState;
class networkComponent;
class node;

class Reliability {
public:
    Reliability();
    Reliability(std::list<node*>* ,node*);
    Reliability(topologyInfo *, std::list<node*>* ,node*);
    Reliability(topologyInfo *, std::list<node*>* ,node*,int);
    virtual ~Reliability();   

    int m,n;
    double finalReliab;
    topologyInfo* topology;
    std::list<networkState*> *states, *mProbStates;
    std::list<node*>* dest;
    node* starter;
    
    double finalReliability(std::list<networkState*>* s, topologyInfo* t);
    double finalReliabilityNew(std::list<networkState*>* s, topologyInfo* t);
    
    int numberOfCommunicateNodes(networkState* n, topologyInfo* topo);
    int CommDestination(networkState* n, topologyInfo* topo);

private:
    int fatorial(int n, int stop);
    double comb(int n, int p);
    std::vector<int> intToBinary(int number) ;
    long binaryToInt(std::vector<networkComponent*> vet);
    std::list<networkState*>* generateNetworkStates(topologyInfo* t);
    std::list<networkState*>* generateNetworkStatesNew(topologyInfo* t);
    topologyInfo* adjustTopology(networkState* n, topologyInfo* topo);
    std::list<networkState*>* orderAlgorithm(std::list<networkState*>* u, int m);
    int numberMostProbableStates(int L);
    std::list<networkState*>* AsetaB(std::list<networkState*>* A, std::list<networkState*>* B);
    std::list<networkState*>* appendAi(std::list<networkState*>* A, int i);
    std::list<networkState*>* TmA(std::list<networkState*>* A, int m);
    //void calculateStateProbability(networkState* n);
    
};

#endif	/* RELIABILITY_H */
