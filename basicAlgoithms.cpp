/* 
 * File:   basicAlgoithms.cpp
 * Author: rafaellgom
 * 
 * Created on December 19, 2016, 9:49 AM
 */

#include "basicAlgoithms.h"

basicAlgoithms::basicAlgoithms() {
}

basicAlgoithms::basicAlgoithms(const basicAlgoithms& orig) {
}

basicAlgoithms::~basicAlgoithms() {
}

std::list<linkInfo*>* basicAlgoithms::extractLinks(std::map<node*, pathInfo*>* first){
    std::list<linkInfo*> * final = new std::list<linkInfo*>();

    //merge o conj de caminhos
    for (std::map<node*, pathInfo*>::iterator i = first->begin(); i != first->end(); i++) {
        for (std::list<linkInfo*>::iterator j = i->second->linkSequence.begin(); j != i->second->linkSequence.end(); j++) {
            bool found = false;
            for (std::list<linkInfo*>::iterator k = final->begin(); k != final->end(); k++) {
                if (((*k)->to->id == (*j)->to->id) && ((*k)->from->id == (*j)->from->id)) {
                    //if ((*k)->id == (*j)->id)  {
                    found = true;
                    break;
                } 
            }
            if (found == false) {
                final->push_back(*j);
            }
        }
    }
    return final;
}

std::list<linkInfo*> * basicAlgoithms::mergePaths(std::map<node*, pathInfo*>* first, std::map<node*, pathInfo*>* second, std::vector<double> bwReq) {

    std::list<linkInfo*> * final = new std::list<linkInfo*>();
    //printf("\nfirst:");
    //merge o conj de caminhos
    for (std::map<node*, pathInfo*>::iterator i = first->begin(); i != first->end(); i++) {
        for (std::list<linkInfo*>::iterator j = i->second->linkSequence.begin(); j != i->second->linkSequence.end(); j++) {
            bool found = false;
            
            for (std::list<linkInfo*>::iterator k = final->begin(); k != final->end(); k++) {
                if (((*k)->to->id == (*j)->to->id) && ((*k)->from->id == (*j)->from->id)) {
                    //if ((*k)->id == (*j)->id)  {
                    
                    found = true;
                    break;
                } else {
                }
            }
            if (found == false) {
                (*j)->allocatedBw = bwReq; // / 2;
                final->push_back(*j);
                //printf("%d -> %d \t", (*j)->to->id, (*j)->from->id);
            }
        }
    }
    //printf("\n");

    //printf("second:");
    for (std::map<node*, pathInfo*>::iterator i = second->begin(); i != second->end(); i++) {
        for (std::list<linkInfo*>::iterator j = i->second->linkSequence.begin(); j != i->second->linkSequence.end(); j++) {
            bool found = false;
            for (std::list<linkInfo*>::iterator k = final->begin(); k != final->end(); k++) {
                if (((*k)->to->id == (*j)->to->id) && ((*k)->from->id == (*j)->from->id)) {
                    //if ((*k)->id == (*j)->id)  {
                    found = true;
                    break;
                }
            }
            if (found == false) {
                (*j)->allocatedBw = bwReq;// /2
                final->push_back(*j);
                //printf("%d -> %d \t", (*j)->to->id, (*j)->from->id);
            }
        }
    }//printf("\n");

    for (std::list<linkInfo*>::iterator iFinal = final->begin(); iFinal != final->end(); iFinal++) {
        for (std::list<linkInfo*>::iterator i = linkList->begin(); i != linkList->end(); i++) {
            if (
                    (((*iFinal)->to->id == (*i)->to->id) && ((*iFinal)->from->id == (*i)->from->id)) ||
                    (((*iFinal)->to->id == (*i)->from->id) && ((*iFinal)->from->id == (*i)->to->id))
                    ) {
                //if ((*iFinal)->id == (*i)->id)  {
                (*i)->allocatedBw = (*iFinal)->allocatedBw;
                (*iFinal) = (*i);
            }
        }
    }


    return final;
}

topologyInfo * basicAlgoithms::extractTopologyInfo(std::list<linkInfo*>* linkList) {
    topologyInfo* t = new topologyInfo();
    t->links = linkList; //new std::list<linkInfo *>();
    t->nodes = new std::list<node *>();

    for (std::list<linkInfo*>::iterator i = linkList->begin(); i != linkList->end(); i++) {
        bool foundFrom = false;
        bool foundTo = false;

        for (std::list<node*>::iterator j = t->nodes->begin(); j != t->nodes->end(); j++) {
            if ((*i)->from->id == (*j)->id) {
                foundFrom = true;
                if (foundTo == true)
                    break;
            }

            if ((*i)->to->id == (*j)->id) {
                foundTo = true;
                if (foundFrom == true)
                    break;
            }
        }

        if (foundFrom == false) {
            t->nodes->push_back((*i)->from);
        }

        if (foundTo == false) {

            t->nodes->push_back((*i)->to);
        }

    }

    return t;
}

std::map<node*, pathInfo*>* basicAlgoithms::shortestPath(node* s, std::list<node*>* dest, double request, int time) {

    std::list<node*> * nonprocessed = new std::list<node*>();
    std::list<node*> * processed = new std::list<node*>();
    distance->clear();
    std::map<node*, node*>* parent = new std::map<node*, node*>();

    //printf("Short path: %d \n", s->neighbors->size());

    //inicializa os nos nao processados e as distancias iniciais
    for (std::list<node*>::iterator itNode = nodesInfo->begin(); itNode != nodesInfo->end(); itNode++) {
        //printf("Node: %s \n", (*itNode)->name);
        bool found = false;
        for (std::list<linkInfo*>::iterator itLink = linkList->begin(); itLink != linkList->end(); itLink++) {
            //printf("- f %s t %s w %f \n", (*itLink)->from->name, (*itLink)->to->name, (*itLink)->weight[time]);
            if ((*itLink)->from->id == s->id && (*itLink)->to->id == (*itNode)->id
                    //|| (*itLink)->to == s && (*itLink)->from == &(*itNode)
                    ) {
                if ((*itLink)->bw[time] < request) {
                    distance->insert(std::pair<node*, double>((*itNode), MAXINT));
                } else {
                    distance->insert(std::pair<node*, double>((*itNode), (*itLink)->weight[time]));
                    
                }

                parent->insert(std::pair<node*, node*>((*itNode), s));
                found = true;
            }
        }//fim do for Link     
        if (found == false && (*itNode) != s) {
            distance->insert(std::pair<node*, double>((*itNode), MAXINT));
        }
    }//fim do for Node

    distance->insert(std::pair<node*, double>(s, 0));

    node * currentNode = s;
    nonprocessed->push_back(currentNode);

    while (nonprocessed->empty() == false) {
        processed->push_back(currentNode);
        for (std::list<node*>::iterator it = currentNode->neighbors->begin(); it != currentNode->neighbors->end(); it++) {
            bool alreadyProc = false;
            for (std::list<node*>::iterator j = processed->begin(); j != processed->end(); j++) {
                //                printf(" j %d \n", (*j)->id);
                //                printf(" it %d \n", (*it)->id);
                if ((*j)->id == (*it)->id) {
                    alreadyProc = true;
                    break;
                }
            }//fim do for j
            if (alreadyProc == false) {
                nonprocessed->push_back(*it);
            }
        }//fim do for it
        nonprocessed->remove(currentNode);


        double bestCost = MAXINT;
        //pega o noh nao processado com o menor custo
        for (std::list<node*>::iterator i = nonprocessed->begin(); i != nonprocessed->end(); i++) {
            if (bestCost >= distance->at((*i))) {
                currentNode = (*i);
                bestCost = distance->at((*i));
            }
        }

        for (std::list<node*>::iterator itNode = nodesInfo->begin(); itNode != nodesInfo->end(); itNode++) {
            for (std::list<linkInfo*>::iterator itLink = linkList->begin(); itLink != linkList->end(); itLink++) {

                if ((*itLink)->from == currentNode && (*itLink)->to == (*itNode)
                        //||( (*itLink)->to == currentNode && (*itLink)->from == (*itNode) )
                        ) {//printf("wl (%d -> %d) %f \n",(*itLink)->id, time,(*itLink)->weight[time]);
                    if ((distance->at((*itNode)) >= bestCost + (*itLink)->weight[time]) && (*itLink)->bw[time] >= request) {
                        distance->at((*itNode)) = bestCost + (*itLink)->weight[time];
                        parent->erase((*itNode));
                        parent->insert(std::pair<node*, node*>((*itNode), currentNode));
                    }
                }
            }//fim do for Link
        }//fim do for Node
    }

    //        printf("\n------------------------\n");
    //        for (std::map<node *, double>::iterator it = distance->begin(); it != distance->end(); it++) {
    //            printf("%s :: %f \n", it->first->name, it->second);
    //        }
    //        printf("END: shortestPath \n-------------------\n");

    std::map<node*, pathInfo*>* paths = findPaths(s, dest, parent, time);

    return paths;
}

std::map<node*, pathInfo*>* basicAlgoithms::findPaths(node* s, std::list<node*>* dest, std::map<node*, node*>* parent, int time) {

    std::map<node*, pathInfo*>* paths = new std::map<node*, pathInfo* > ();
    node * currentNode = s;
    std::list<linkInfo*> * auxlinkList = new std::list<linkInfo*>();

    for (std::map<node *, double>::iterator it = distance->begin(); it != distance->end(); it++) {

        bool isDest = false;
        for (std::list<node*>::iterator d = dest->begin(); d != dest->end(); d++) {
            if (it->first == (*d))
                isDest = true;
        }
        if (isDest == false)
            continue;

        pathInfo * p = new pathInfo;
        p->totalWeight = it->second;
        p->hops = 0;
        paths->insert(std::pair<node*, pathInfo* > ((it->first), p));
    }

    //    for (std::map<node*, node*>::iterator it = parent->begin(); it != parent->end(); it++) {
    //        printf("\t %s \t %s \n", (it)->first->name, (it)->second->name);
    //    }

    for (std::map<node*, node*>::iterator it = parent->begin(); it != parent->end(); it++) {

        bool isDest = false;
        for (std::list<node*>::iterator d = dest->begin(); d != dest->end(); d++) {
            if (it->first == (*d))
                isDest = true;
        }
        if ((isDest == false))
            continue;

        currentNode = it->first;
        paths->at(it->first)->nodesSequence.push_back(currentNode);
        paths->at(it->first)->totalWeight += distance->at(currentNode);

        while (currentNode != s) {
            //printf("while %s - %s\n", currentNode->name , parent->at(currentNode)->name );
            currentNode = parent->at(currentNode);
            paths->at(it->first)->hops++;
            paths->at(it->first)->nodesSequence.push_back(currentNode);

            linkInfo* l = new linkInfo;
            std::list<node*>::iterator aux;
            for (aux = paths->at(it->first)->nodesSequence.begin(); aux != paths->at(it->first)->nodesSequence.end(); aux++) {
                if ((*aux) == currentNode)
                    break;
            }
            aux--;
            l->to = (*(aux));
            l->from = currentNode;
            //l->weight[time] = 0;
            l->timesUsed = 1;
            l->hop = paths->at(it->first)->hops;
            paths->at(it->first)->linkSequence.push_back(l);

            bool found = false;
            for (std::list<linkInfo*>::iterator itLink = auxlinkList->begin(); itLink != auxlinkList->end(); itLink++) {
                if (l->from->id == (*itLink)->from->id && l->to->id == (*itLink)->to->id) {
                    found = true;
                    (*itLink)->timesUsed++;
                }
            }
            if (found == false) {

                auxlinkList->push_back(l);
            }

        }//fim do while

    }//fim do for it

    for (std::map<node*, pathInfo*>::iterator it = paths->begin(); it != paths->end(); it++) {
        if ((it)->second->nodesSequence.size() > 0) {
            (it)->second->ok = true;
            //printf("(%d) %s :: %d %f :: ", time, (it)->first->name, (it)->second->hops, (it)->second->totalWeight);
            //for (std::list<node*>::iterator i = (it)->second->nodesSequence.begin(); i != (it)->second->nodesSequence.end(); i++) {
            //    printf("%s ", (*i)->name);
            //}
            //printf("%d \n", (it)->second->ok);
        } else {
            (it)->second->ok = false;
            //printf("%d \n", (it)->second->ok);
        }
        //printf("\n");
    }
    //
    //    printf("END: findPath \n-------------------\n");

    return paths;
}


