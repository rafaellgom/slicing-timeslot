/* 
 * File:   RegionRisk.cpp
 * Author: rafaellgom
 * 
 * Created on November 18, 2014, 2:46 PM
 */

#include "RegionRisk.h"
#include "reliability.h"

RegionRisk::RegionRisk(std::vector<networkComponent*> f, topologyInfo* t) {
    failuresOrder = f;
    infrastructure = t;
}

RegionRisk::~RegionRisk() {
}

double RegionRisk::commUnderDisaster(bool solved) {
    return -1;
}

double RegionRisk::commUnderDisaster(char * regionName, topologyInfo* t, double failPercentage, std::list<node*>* d, node* root) {

    networkState* s = new networkState();
    int remainingFails = failPercentage * numberOfComponentsInRegion(regionName, infrastructure);
    
    for (int i = 0; i < failuresOrder.size(); i++) {
        if ((remainingFails != 0)) {
            if (
                    (isDestination(failuresOrder[i], d) == false) &&
                    (failuresOrder[i]->type == LINK)
                    ) {
                (failuresOrder[i])->active = 1;
                remainingFails--;
            }
        } else {
            (failuresOrder[i])->active = 0;
        }

        s->state.push_back(failuresOrder[i]);

    }
    Reliability* r = new Reliability(d, root);

    double comm = (double) r->CommDestination(s, t);
    
    //caso queira o percentual de comm
    //return comm / (double) d->size();
    if(comm == (double) d->size() ){
        return 1;
    } else{
        return 0;
    }
}

bool RegionRisk::isDestination(networkComponent* no, std::list<node*>* d) {
    for (std::list<node*>::iterator i = d->begin(); i != d->end(); i++) {
        if ((*i)->id == no->id) {
            return true;
        }
    }
    return false;
}

int RegionRisk::numberOfComponentsInRegion(char * regionName, topologyInfo* t) {
    int number = 0;

    for (std::list<linkInfo*>::iterator i = t->links->begin(); i != t->links->end(); i++) {
        if (((strcmp((*i)->from->region, regionName) == 0) || (strcmp((*i)->to->region, regionName) == 0))
                || ((strcmp("0", regionName) == 0) || (strcmp("0", regionName) == 0))
                ) {

            number++;
        }
    }

    return number;
}