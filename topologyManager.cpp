/* 
 * File:   TopologyManager.cpp
 * Author: rafaellgom
 * 
 * Created on July 16, 2012, 3:45 PM
 */

#include <math.h>
#include <libxml2/libxml/tree.h>
#include "topologyManager.h"
#include "reliability.h"
#include "ResquestGeneration.h"
#include "TimeSlotAlgorithms.h"
#include "basicAlgoithms.h"

char* itoa(int val, int base) {
    static char buf[32] = {0};
    int i = 30;
    for (; val && i; --i, val /= base)
        buf[i] = "0123456789abcdef"[val % base];
    return &buf[i + 1];
}

void reverse_string(char str[]) {
    char c;
    char *p, *q;

    p = str;
    if (!p)
        return;

    q = p + 1;
    if (*q == '\0')
        return;

    c = *p;
    reverse_string(q);

    while (*q != '\0') {
        *p = *q;
        p++;
        q++;
    }
    *p = c;

    return;
}

topologyManager::topologyManager(char * file, int a, int d, int s) {
    filename = file;
    nodesInfo = new std::list<node *> ();
    linkList = new std::list<linkInfo*>();
//    objMetric = a;
//    reliabilityApproach = d;
//    timeSlotAlgorithm = s;
    weightEnergy = 0.5;
    readXml();
}

topologyManager::topologyManager(char * file) {
    filename = file;
    nodesInfo = new std::list<node *> ();
    linkList = new std::list<linkInfo*>();
    weightEnergy = 0.5;
    readXml();
}

topologyManager::~topologyManager() {

}

linkInfo* topologyManager::updateLinkWeight(linkInfo* l, int algorithm, std::vector<double> request) {

    double t, en = 0;
    for (int i = 0; i < request.size(); i++) {
        switch (algorithm) {
            case LOWEST_WEIGHTED_PATH:
                l->weight[i] = 10 * (1 - (l->bw[i] / request[i]));
                if (l->weight[i] <= 0) {
                    l->weight[i] = 1;
                }
                break;
            case MAX_AVAL_BW:
                l->weight[i] = l->bw[i];
                break;
            case FEASIBLE_BW:
                l->weight[i] = 1 + 10*((request[i] / l->bw[i]));
                break;
            case BW_LOG:
                if (request[i] <= l->bw[i]) {
                    l->weight[i] = log(l->bw[i] / request[i]);
                } else
                    l->weight[i] = MAXINT;
                break;
            case BW_RISK_RATIO:
                t = request[i] / l->bw[i];
                if (t < 1) {
                    l->weight[i] = log(l->bw[i] / request[i]) + l->regionRisk;
                } else
                    l->weight[i] = 10;
                break;
            case DBW:
                if (l->bw[i] >= request[i]) {
                    l->weight[i] = 1;
                } else {
                    l->weight[i] = MAXINT;
                }
                break;
            case ENERGY_AWARE:
                if (l->energy != 0)
                    l->weight[i] = (getLinkEnergyConsumption(l->bw[i]) / getLinkEnergyConsumption(l->bw[i] + request[i]));
                else
                    l->weight[i] = 2;
                break;
            case BW_ENERGY_MIX:
                if (l->energy != 0) {
                    en = (getLinkEnergyConsumptionNew(l->bw[i]) / getLinkEnergyConsumptionNew(l->bw[i] + request[i]));
                    //l->weight = (getLinkEnergyConsumption(l->bw) / getLinkEnergyConsumption(l->bw + request));
                } else {
                    en = 1;
                }
                l->weight[i] = (1 + 10 * ((request[i] / l->bw[i]))) + (en);
                break;

            case DA_BW_ENERGY:
                if (l->energy != 0)
                    en = (getLinkEnergyConsumption(l->allocatedBw[i]) / getLinkEnergyConsumption(l->allocatedBw[i] + request[i]));
                else
                    en = 1;
                l->weight[i] = (1 - weightEnergy) *(10 * ((request[i] / l->bw[i]))) + (weightEnergy * en);
                break;
            default:
                l->weight[i] = l->bw[i];
                break;
        }
    }


    return l;
}

std::list<topologyInfo*>* topologyManager::generateSlices(node* r, std::list<node*>* destNodes, double reliability, 
        std::vector<double> bwRequest, int *numberOfMigration, int objMetric, int reliabilityApproach, int timeSlotAlgorithm) {
    //******************************
    updatedlinkList = new std::list<linkInfo*>();
    auxlinkList = new std::list<linkInfo*>();
    distance = new std::map<node*, double>();
    //******************************

    root = getNodeInfoByName(r->name);
    dest = new std::list<node*>();
    for (std::list<node *>::iterator i = destNodes->begin(); i != destNodes->end(); i++) {
        dest->push_back(getNodeInfoByName((*i)->name));
    }

    for (std::list<linkInfo*>::iterator it = linkList->begin(); it != linkList->end(); it++) {
        for (int time = 0; time < bwRequest.size(); time++) {
            if ((*it)->bw[time] < bwRequest[time]) {
                continue;
            }
        }
        linkInfo* l = new linkInfo();
        *l = *(*it);
        updatedlinkList->push_back(updateLinkWeight(l, objMetric, bwRequest));
    }

    TimeSlotAlgorithms* timeSlotAlg = new TimeSlotAlgorithms(updatedlinkList, nodesInfo);//, timeSlotAlgorithm, reliabilityApproach, numberOfMigration);

    std::list<topologyInfo*>* dayAllocation = timeSlotAlg->defineTopology(root, dest, reliability, 
            bwRequest, numberOfMigration, timeSlotAlgorithm, reliabilityApproach);
    
    delete timeSlotAlg;
    delete updatedlinkList;
    delete auxlinkList;
    delete distance;

    return dayAllocation;
}

/*
double topologyManager::getReliability(topologyInfo* topology, std::list<node*>* dest, node* root, int fails) {
    Reliability *r = new Reliability(topology, dest, root, fails);
    double aux = r->finalReliab;
    delete r;

    return aux;
}
 */

void topologyManager::readXml() {
    xmlDocPtr doc;
    xmlNodePtr cur;
    doc = xmlParseFile(filename);

    //printf("******************\n");
    //printf("Updating the avalible resources...\n");

    if (doc == NULL) {
        fprintf(stderr, "Document not parsed successfully. \n");
        //return;
    }

    cur = xmlDocGetRootElement(doc);

    if (cur == NULL) {
        fprintf(stderr, "empty document\n");
        xmlFreeDoc(doc);
        //return;
    }

    if (xmlStrcmp(cur->name, (const xmlChar *) "topology")) {
        fprintf(stderr, "document of the wrong type, root node != story");
        xmlFreeDoc(doc);
        //return;
    }

    cur = cur->xmlChildrenNode;
    while (cur != NULL) {
        if ((!xmlStrcmp(cur->name, (const xmlChar *) "node"))) {
            parseNode(doc, cur);
        }

        if ((!xmlStrcmp(cur->name, (const xmlChar *) "link"))) {

            parseLink(doc, cur);
        }

        cur = cur->next;
    }

    xmlFreeNodeList(cur);

}

void topologyManager::parseNode(xmlDocPtr doc, xmlNodePtr cur) {
    xmlChar *id, *type, *name, *link, *bw, *reliability, * reliab, *rRisk, *region;

    id = xmlGetProp(cur, (const xmlChar *) "id");
    type = xmlGetProp(cur, (const xmlChar *) "type");
    name = xmlGetProp(cur, (const xmlChar *) "name");
    reliability = xmlGetProp(cur, (const xmlChar *) "reliability");
    rRisk = xmlGetProp(cur, (const xmlChar *) "regionRisk");
    region = xmlGetProp(cur, (const xmlChar *) "region");

    node n;
    n.id = atoi((char *) id);
    n.type = atoi((char *) type);
    n.name = (char *) name;
    n.region = (char *) region;
    n.reliability = atof((char *) reliability);
    n.energy = 0;
    n.regionRisk = atof((char *) rRisk);

    node* src = nodeRegistred(n);
}

void topologyManager::parseLink(xmlDocPtr doc, xmlNodePtr cur) {
    xmlChar *from, *to, *bw, *reliability, *rRisk, *region;

    from = xmlGetProp(cur, (const xmlChar *) "from");
    to = xmlGetProp(cur, (const xmlChar *) "to");
    bw = xmlGetProp(cur, (const xmlChar *) "bw");
    reliability = xmlGetProp(cur, (const xmlChar *) "reliability");
    rRisk = xmlGetProp(cur, (const xmlChar *) "regionRisk");
    region = xmlGetProp(cur, (const xmlChar *) "region");

    node nF;
    nF.id = atoi((char *) to);
    nF.name = (char *) "empty";
    node* src = nodeRegistred(nF);

    node nT;
    nT.id = atoi((char *) from);
    nT.name = (char *) "empty";
    node* dst = nodeRegistred(nT);

    if (strcmp(src->name, "root") == 0) {
        add_edge(dst, src, atof((char *) bw), atof((char *) reliability), atof((char *) rRisk), (char *) region);
    } else {

        add_edge(dst, src, atof((char *) bw), atof((char *) reliability), atof((char *) rRisk), (char *) region);
    }

    free(from);
    free(to);
    free(bw);
    free(reliability);
    free(rRisk);
    free(region);
}

void topologyManager::add_edge(node* dest_node, node* last_node, double bw, double r, double risk, char * region) {

    for (std::list<linkInfo*>::iterator it = linkList->begin(); it != linkList->end(); it++) {
        if ((((*it)->from == last_node) && ((*it)->to) == dest_node) ||
                (((*it)->from == dest_node) && ((*it)->to) == last_node)) {
            return;
        }
    }

    linkInfo *l = new linkInfo;
    char linkId[10];
    char linkIdReverse[10];

    strcpy(linkId, (char *) itoa(last_node->id, 10));
    if (dest_node->id == 0) {
        strcat(linkId, (char *) "00"); //n mais usado
    } else {
        strcat(linkId, (char *) "0");
        strcat(linkId, (char *) itoa(dest_node->id, 10));
    }

    l->id = atoi(linkId);
    l->from = last_node;
    l->to = dest_node;
    l->originalBw = bw;
    for (int i = 0; i < NUMBER_OF_SLOTS; i++) {
        l->weight.push_back(1);
        l->bw.push_back(bw);
        l->allocatedBw.push_back(0);
    }
    l->reliability = r;
    l->timesUsed = 0;
    l->hop = 0;
    l->energy = 0;
    l->regionRisk = risk;
    l->type = LINK;
    l->region = region;
    linkList->push_back(l);

    //old
    //reverse_string(linkId);
    strcpy(linkIdReverse, (char *) itoa(dest_node->id, 10));
    if (dest_node->id == 0) {
        strcat(linkIdReverse, (char *) "00"); //n mais usado
    } else {
        strcat(linkIdReverse, (char *) "0");
        strcat(linkIdReverse, (char *) itoa(last_node->id, 10));
    }

    linkInfo *lb = new linkInfo;
    lb->id = atoi(linkIdReverse);
    lb->from = dest_node;
    lb->to = last_node;
    lb->originalBw = bw;
    for (int i = 0; i < NUMBER_OF_SLOTS; i++) {
        lb->weight.push_back(1);
        lb->bw.push_back(bw);
        lb->allocatedBw.push_back(0);
    }
    lb->reliability = r;
    lb->timesUsed = 0;
    lb->hop = 0;
    lb->energy = 0;
    lb->regionRisk = risk;
    lb->type = LINK;
    lb->region = region;
    linkList->push_back(lb);

    last_node->neighbors->push_back(dest_node);
    dest_node->neighbors->push_back(last_node);
}

/*
std::map<node*, pathInfo*>* topologyManager::dijkstra(std::list<linkInfo*> * linkList, node * s, std::list<node*>* dest, double request) {

    std::list<node*> * nonprocessed = new std::list<node*>();
    std::list<node*> * processed = new std::list<node*>();
    distance->clear();
    std::map<node*, double>* dAux = new std::map<node*, double>();
    std::map<node*, node*>* parent = new std::map<node*, node*>();

    //inicializa os nos nao processados e as distancias iniciais
    for (std::list<node*>::iterator itNode = nodesInfo->begin(); itNode != nodesInfo->end(); itNode++) {
        bool found = false;
        for (std::list<linkInfo*>::iterator itLink = linkList->begin(); itLink != linkList->end(); itLink++) {
            //printf("- f %s t %s w %f \n", (*itLink)->from->name, (*itLink)->to->name, (*itLink)->weight);
            if ((*itLink)->from == s && (*itLink)->to == (*itNode)
                    //|| (*itLink)->to == s && (*itLink)->from == &(*itNode)
                    ) {
                switch (algorithm) {
                    case MAX_AVAL_BW:
                        if ((*itLink)->bw < request) {
                            distance->insert(std::pair<node*, double>((*itNode), 0));
                            dAux->insert(std::pair<node*, double>((*itNode), 0));
                        } else {
                            distance->insert(std::pair<node*, double>((*itNode), (*itLink)->weight));
                        }
                        break;
                    default:
                        if ((*itLink)->bw < request)
                            distance->insert(std::pair<node*, double>((*itNode), MAXINT));
                        else
                            distance->insert(std::pair<node*, double>((*itNode), (*itLink)->weight));
                        break;
                }
                parent->insert(std::pair<node*, node*>((*itNode), s));
                found = true;
                break;
            }
        }//fim do for Link     
        if (found == false && (*itNode) != s) {
            switch (algorithm) {
                case MAX_AVAL_BW:
                    distance->insert(std::pair<node*, double>((*itNode), 0));
                    break;
                default:
                    distance->insert(std::pair<node*, double>((*itNode), MAXINT));
                    break;
            }
        }
    }//fim do for Node

    distance->insert(std::pair<node*, double>(s, 0));
    node * currentNode = s;
    nonprocessed->push_back(currentNode);

    while (nonprocessed->empty() == false) {
        processed->push_back(currentNode);
        for (std::list<node*>::iterator it = currentNode->neighbors.begin(); it != currentNode->neighbors.end(); it++) {
            bool alreadyProc = false;
            for (std::list<node*>::iterator j = processed->begin(); j != processed->end(); j++) {
                if ((*j)->id == (*it)->id) {
                    alreadyProc = true;
                    break;
                }
            }//fim do for j
            if (alreadyProc == false) {
                nonprocessed->push_back(*it);
            }
        }//fim do for it

        nonprocessed->remove(currentNode);
        double bestCost;
        switch (algorithm) {
            case LOWEST_WEIGHTED_PATH:
                bestCost = MAXINT;
                break;
            case FEASIBLE_BW:
                bestCost = MAXINT;
                break;
            case MAX_AVAL_BW:
                bestCost = -1;
                break;
            case BW_RISK_RATIO:
                bestCost = MAXINT;
                break;
            default:
                bestCost = MAXINT;
                break;
        }

        //pega o noh nao processado com o menor custo
        for (std::list<node*>::iterator i = nonprocessed->begin(); i != nonprocessed->end(); i++) {
            switch (algorithm) {
                case MAX_AVAL_BW:
                    if (bestCost <= distance->at((*i))) {
                        currentNode = (*i);
                        bestCost = distance->at((*i));
                    }
                    break;
                default:
                    if (bestCost >= distance->at((*i))) {
                        currentNode = (*i);
                        bestCost = distance->at((*i));
                    }
                    break;
            }

        }

        for (std::list<node*>::iterator itNode = nodesInfo->begin(); itNode != nodesInfo->end(); itNode++) {
            for (std::list<linkInfo*>::iterator itLink = linkList->begin(); itLink != linkList->end(); itLink++) {

                if ((*itLink)->from == currentNode && (*itLink)->to == (*itNode)
                        //|| (*itLink)->to == currentNode && (*itLink)->from == &(*itNode)
                        ) {
                    switch (algorithm) {
                        case LOWEST_WEIGHTED_PATH:
                            if (distance->at((*itNode)) > bestCost + (*itLink)->weight) {
                                distance->at((*itNode)) = bestCost + (*itLink)->weight;
                                parent->erase((*itNode));
                                parent->insert(std::pair<node*, node*>((*itNode), currentNode));
                            }
                            break;
                        case FEASIBLE_BW:
                            if (distance->at((*itNode)) > bestCost + (*itLink)->weight) {
                                distance->at((*itNode)) = bestCost + (*itLink)->weight;
                                parent->erase((*itNode));
                                parent->insert(std::pair<node*, node*>((*itNode), currentNode));
                            }
                            break;
                        case MAX_AVAL_BW:
                            if (distance->at((*itNode)) < min(bestCost, (*itLink)->weight)) {
                                distance->at((*itNode)) = min(bestCost, (*itLink)->weight);
                                parent->erase((*itNode));
                                parent->insert(std::pair<node*, node*>((*itNode), currentNode));
                            }
                            break;
                        default:
                            if ((distance->at((*itNode)) >= bestCost + (*itLink)->weight) && (*itLink)->bw >= request) {
                                distance->at((*itNode)) = bestCost + (*itLink)->weight;
                                parent->erase((*itNode));
                                parent->insert(std::pair<node*, node*>((*itNode), currentNode));
                            }
                            break;
                    }
                }
            }//fim do for Link
        }//fim do for Node

    }//fim do while nonprocessed

    //**************************************************************************

    // <editor-fold defaultstate="collapsed" desc="comment">

    std::map<node*, pathInfo*>* paths = new std::map<node*, pathInfo* > ();

    for (std::map<node *, double>::iterator it = distance->begin(); it != distance->end(); it++) {

        bool isDest = false;
        for (std::list<node*>::iterator d = dest->begin(); d != dest->end(); d++) {
            if (it->first == (*d))
                isDest = true;
        }
        if (isDest == false)
            continue;

        pathInfo * p = new pathInfo;
        p->totalWeight = it->second;
        p->hops = 0;
        paths->insert(std::pair<node*, pathInfo* > ((it->first), p));
    }

    for (std::map<node*, node*>::iterator it = parent->begin(); it != parent->end(); it++) {

        bool isDest = false;
        for (std::list<node*>::iterator d = dest->begin(); d != dest->end(); d++) {
            if (it->first == (*d))
                isDest = true;
        }
        if ((isDest == false))
            continue;

        currentNode = it->first;
        paths->at(it->first)->nodesSequence.push_back(currentNode);
        paths->at(it->first)->totalWeight += distance->at(currentNode);

        while (currentNode != s) {
            currentNode = parent->at(currentNode);
            paths->at(it->first)->hops++;
            paths->at(it->first)->nodesSequence.push_back(currentNode);

            linkInfo* l = new linkInfo;
            std::list<node*>::iterator aux;
            for (aux = paths->at(it->first)->nodesSequence.begin(); aux != paths->at(it->first)->nodesSequence.end(); aux++) {
                if ((*aux) == currentNode)
                    break;
            }
            aux--;
            l->to = (*(aux));
            l->from = currentNode;
            l->weight = 0;
            l->timesUsed = 1;
            l->hop = paths->at(it->first)->hops;
            paths->at(it->first)->linkSequence.push_back(l);

            bool found = false;
            for (std::list<linkInfo*>::iterator itLink = auxlinkList->begin(); itLink != auxlinkList->end(); itLink++) {
                if (l->from->id == (*itLink)->from->id && l->to->id == (*itLink)->to->id) {
                    found = true;
                    (*itLink)->timesUsed++;
                }
            }
            if (found == false) {

                auxlinkList->push_back(l);
            }

        }//fim do while

    }//fim do for it// </editor-fold>

    return paths;
}
 */

/*
void topologyManager::updateLinkList(double percent) {

    int number = (int) (auxlinkList->size() * percent);

    for (std::list<linkInfo*>::iterator itLink = updatedlinkList->begin(); itLink != updatedlinkList->end(); itLink++) {
        (*itLink)->hop = MAXINT;
        for (std::list<linkInfo*>::iterator it = auxlinkList->begin(); it != auxlinkList->end(); it++) {
            if (((*itLink)->from->id == (*it)->from->id) && ((*itLink)->to->id == (*it)->to->id)) {
                (*itLink)->hop = (*it)->hop;
            }
        }
    }

    //ordena de acordo com o salto para atualizar primeiro os links perto do destino
    bool swapped = true;
    std::list<linkInfo*>::iterator j, aux = updatedlinkList->end();
    linkInfo* l;
    while (swapped) {
        swapped = false;
        aux--;
        for (std::list<linkInfo*>::iterator i = updatedlinkList->begin(); i != aux; i++) {
            j = i;
            if (j == updatedlinkList->end())
                break;
            j++;
            if ((*i)->timesUsed < (*j)->timesUsed) {
                l = (*i);
                (*i) = (*j);
                (*j) = l;
                swapped = true;
            }
        }//fim do for i
    }

    int found = 0;
    for (std::list<linkInfo*>::iterator itLink = updatedlinkList->begin(); itLink != updatedlinkList->end(); itLink++) {
        for (std::list<linkInfo*>::iterator it = auxlinkList->begin(); it != auxlinkList->end(); it++) {
            if (
                    (((*itLink)->from->id == (*it)->from->id) && ((*itLink)->to->id == (*it)->to->id))
                    //||
                    //(((*itLink)->to->id == (*it)->from->id) && ((*itLink)->from->id == (*it)->to->id))
                    ) {
                switch (algorithm) {
                    case MAX_AVAL_BW:
                        if (number > 0) {
                            (*itLink)->weight = 0.0000001;
                            number--;
                        } else {
                            (*itLink)->weight = (MAXINT / 100);
                        }
                        break;
                    default:
                        if (number > 0) {
                            (*itLink)->weight = (MAXINT / 100);
                            number--;
                        } else {
                            (*itLink)->weight = 0;
                        }
                        break;
                }

                (*itLink)->used = true;

                found++;
                if (found == 2) {
                    found = 0;
                    break;
                }
            }

        }//fim do for itPath

    }//fim do for itLink

}
 */

/*
std::list<linkInfo*> * topologyManager::mergePaths(std::map<node*, pathInfo*>* first, std::map<node*, pathInfo*>* second, std::vector<double> bwReq) {

    std::list<linkInfo*> * final = new std::list<linkInfo*>();

    //merge o conj de caminhos
    for (std::map<node*, pathInfo*>::iterator i = first->begin(); i != first->end(); i++) {
        for (std::list<linkInfo*>::iterator j = i->second->linkSequence.begin(); j != i->second->linkSequence.end(); j++) {
            bool found = false;
            for (std::list<linkInfo*>::iterator k = final->begin(); k != final->end(); k++) {
                if (((*k)->to->id == (*j)->to->id) && ((*k)->from->id == (*j)->from->id)) {
                    //if ((*k)->id == (*j)->id)  {
                    found = true;
                    break;
                } else {
                }
            }
            if (found == false) {
                (*j)->allocatedBw = bwReq; // / 2;
                final->push_back(*j);
            }
        }
    }

    for (std::map<node*, pathInfo*>::iterator i = second->begin(); i != second->end(); i++) {
        for (std::list<linkInfo*>::iterator j = i->second->linkSequence.begin(); j != i->second->linkSequence.end(); j++) {
            bool found = false;
            for (std::list<linkInfo*>::iterator k = final->begin(); k != final->end(); k++) {
                if (((*k)->to->id == (*j)->to->id) && ((*k)->from->id == (*j)->from->id)) {
                    //if ((*k)->id == (*j)->id)  {
                    found = true;
                    break;
                }
            }
            if (found == false) {
                (*j)->allocatedBw = bwReq; // / 2;
                final->push_back(*j);
            }
        }
    }

    for (std::list<linkInfo*>::iterator iFinal = final->begin(); iFinal != final->end(); iFinal++) {
        for (std::list<linkInfo*>::iterator i = linkList->begin(); i != linkList->end(); i++) {
            if (
                    (((*iFinal)->to->id == (*i)->to->id) && ((*iFinal)->from->id == (*i)->from->id)) ||
                    (((*iFinal)->to->id == (*i)->from->id) && ((*iFinal)->from->id == (*i)->to->id))
                    ) {
                //if ((*iFinal)->id == (*i)->id)  {
                (*i)->allocatedBw = (*iFinal)->allocatedBw;
                (*iFinal) = (*i);
            }
        }
    }

    return final;
}
 */

topologyInfo * topologyManager::extractTopologyInfo(std::list<linkInfo*>* linkList) {
    topologyInfo* t = new topologyInfo();
    t->links = linkList; //new std::list<linkInfo *>();
    t->nodes = new std::list<node *>();

    for (std::list<linkInfo*>::iterator i = linkList->begin(); i != linkList->end(); i++) {
        bool foundFrom = false;
        bool foundTo = false;

        for (std::list<node*>::iterator j = t->nodes->begin(); j != t->nodes->end(); j++) {
            if ((*i)->from->id == (*j)->id) {
                foundFrom = true;
                if (foundTo == true)
                    break;
            }

            if ((*i)->to->id == (*j)->id) {
                foundTo = true;
                if (foundFrom == true)
                    break;
            }
        }

        if (foundFrom == false) {
            t->nodes->push_back((*i)->from);
        }

        if (foundTo == false) {

            t->nodes->push_back((*i)->to);
        }

    }

    return t;
}

/*
bool topologyManager::fitsResources(topologyInfo* topology) {

    if (topology->numberUnreliable < 1)
        return false;

    for (std::list<linkInfo*>::iterator j = linkList->begin(); j != linkList->end(); j++) {
        for (std::list<linkInfo*>::iterator i = topology->links->begin(); i != topology->links->end(); i++) {
            if ((*j)->bw < (*i)->allocatedBw)

                return false;
        }

    }

    return true;
}
 */

/*
double topologyManager::bwUsage(topologyInfo* topology) {
    double sum = 0;

    for (std::list<linkInfo*>::iterator i = topology->links->begin(); i != topology->links->end(); i++) {

        sum += (*i)->allocatedBw;
    }

    return sum / 2; //return in GBps
}
 */

/*
double topologyManager::bwUsage() {
    double sum = 0;

    for (std::list<linkInfo*>::iterator i = linkList->begin(); i != linkList->end(); i++) {

        sum += (*i)->allocatedBw;
    }

    return sum; ///1000;//return in GBps
}
 */


void topologyManager::updateTopologyResources(std::list<topologyInfo*>* topologiesDay) {

    int time = 0;
    for (std::list<topologyInfo*>::iterator topo = topologiesDay->begin(); topo != topologiesDay->end(); topo++) {

        //printf("Alocando %d (%d) \n", (*topo)->links->size(), time);
        for (std::list<linkInfo*>::iterator i = (*topo)->links->begin(); i != (*topo)->links->end(); i++) {

            for (std::list<linkInfo*>::iterator j = linkList->begin(); j != linkList->end(); j++) {
                if (((*i)->from == ((*j)->from) && ((*i)->to) == ((*j)->to))
                        || (((*i)->from == ((*j)->to)) && ((*i)->to) == (*j)->from)
                        ) {

                    //if ((*j)->bw[time] >= (*i)->allocatedBw[time]) {
                    (*j)->bw[time] -= (*i)->allocatedBw[time];
                    //} else {
                    //   (*j)->bw[time] = 0;
                    //}
                    (*j)->energy = 1;
                    //printf("%f (%d)... \t", (*i)->allocatedBw[time], time);
                    //printf("%f !!!! \n", (*j)->bw[time]);
                }


            }//fim do for j
        }//fim do for i

        for (std::list<node*>::iterator j = nodesInfo->begin(); j != nodesInfo->end(); j++) {
            for (std::list<node*>::iterator i = (*topo)->nodes->begin(); i != (*topo)->nodes->end(); i++) {
                if ((*i)->id == (*j)->id) {
                    (*j)->energy = 1;
                }

            }
        }

        time++;

    }//end for topology
}

/*
double topologyManager::lowerBw(topologyInfo* topology) {
    if (topology->numberUnreliable == 0)
        return 0;

    double lower = (*topology->links->begin())->bw;
    for (std::list<linkInfo*>::iterator i = topology->links->begin(); i != topology->links->end(); i++) {
        if ((*i)->bw < lower) {

            lower = (*i)->bw;
        }
    }
    return lower;
}
 */

void topologyManager::releaseTopologyResources(std::list<topologyInfo*>* topologyAllocated) {
    int time = 0;
    for (std::list<topologyInfo*>::iterator top = topologyAllocated->begin(); top != topologyAllocated->end(); top++) {
        for (std::list<linkInfo*>::iterator i = (*top)->links->begin(); i != (*top)->links->end(); i++) {
            for (std::list<linkInfo*>::iterator j = linkList->begin(); j != linkList->end(); j++) {
                if (((*i)->from == ((*j)->from) && ((*i)->to) == ((*j)->to))
                        || (((*i)->from == ((*j)->to)) && ((*i)->to) == (*j)->from)
                        ) {
                    (*j)->bw[time] += (*i)->allocatedBw[time];
                    if ((*j)->bw[time] >= (*j)->originalBw) {
                        (*j)->energy = 0;
                    }
                }
            }

        }
        time++;
    }

}

topologyInfo::topologyInfo() {
}

topologyInfo::~topologyInfo() {

    for (std::list<linkInfo*>::iterator i = links->begin(); i != links->end(); i++) {
        links->erase(i);
        i--;
    }
    delete links;
    for (std::list<node*>::iterator i = nodes->begin(); i != nodes->end(); i++) {
        nodes->erase(i);
        i--;
    }
    delete nodes;

}
